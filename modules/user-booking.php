<div class="UserBooking">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#profileFlight"><i class="fa fa-plane" aria-hidden="true"></i><br><span>Flights</span></a></li>
		<li><a data-toggle="tab" href="#profileHotel"><i class="fa fa-building" aria-hidden="true"></i><br>Hotels</a></li>
	</ul>
	<div class="clearfix">
	</div>
	<div class="tab-content">
		<div  id="profileFlight" class="tab-pane fade in active">
			<p>Active Booking (0)</p>
			<div class="UBInner1">
				<p>No Active Booking Found</p>
				<p>Anyting you booked shows up here, but it seems like you haven't made any. Let's create one via homepage!</p>
			</div>
			<p>Booking History</p>
			<div class="UBInner2">
				<p>Bali / Denpasar (DPS) <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Jakarta(CGK)</p>
				<div class="pull-right">
					<span>Booking ID</span>
					<span>74306972</span>
				</div>
				<div>
					<span>AirAsia</span>
					<img src="assets/images/airasia-sm.png" alt="">
				</div>

				<div class="UBInner2Grey">
					<table>
						<tr>
							<td>
								FLIGHT SCHEDULE
							</td>
							<td>
								DEPARTURE AIRPORT
							</td>
						</tr>
						<tr>
							<td>
								18 Sep 2018 • 18:25
							</td>
							<td>
								Ngurah Rai Int'l
							</td>
						</tr>
					</table>
				</div>
				<div class="pull-right UBI2More">
					<a href="?page=user/booking-detail-flight">Details</a>
					<a href="?page=user/booking-detail-flight">• • •</a>
				</div>
				<button class="UBI2Ticket">E-TICKET ISSUED</button>
			</div>
		</div>
		<div  id="profileHotel" class="tab-pane fade">
			<p>Active Booking (0)</p>
			<div class="UBInner1">
				<p>No Active Booking Found</p>
				<p>Anyting you booked shows up here, but it seems like you haven't made any. Let's create one via homepage!</p>
			</div>
			<p>Booking History</p>
			<div class="UBInner2">
				<p>Novotel Bogor Golf Resort & Convention Center</p>
				<div class="pull-right">
					<span>Booking ID</span>
					<span>74306972</span>
				</div>
				<div>
					<span>Superior Room, 2 Twin Beds</span>
				</div>

				<div class="UBInner2Grey">
					<table>
						<tr>
							<td>
								CHECK-IN
							</td>
							<td>
								CHECK-OUT
							</td>
						</tr>
						<tr>
							<td>
								28 Jun 2018
							</td>
							<td>
								29 Jun 2018
							</td>
						</tr>
					</table>
				</div>
				<div class="pull-right UBI2More">
					<a href="?page=user/booking-detail-hotel">Details</a>
					<a href="?page=user/booking-detail-hotel">• • •</a>
				</div>
				<button class="UBI2Ticket">HOTEL VOUCHER ISSUED</button>
			</div>

		</div>
	</div>
</div>
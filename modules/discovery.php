<?php 
	if(!isset($pieces[1])){
		include('modules/discovery-country.php');
	}
	elseif ($pieces[1] == 'all') {
		include('modules/discovery-country.php');
	}
	elseif ($pieces[1] == 'continent') {
		include('modules/discovery-continent.php');
	}
	elseif ($pieces[1] == 'detail') {
		include('modules/discovery-detail.php');
	}
	elseif ($pieces[1] == 'attraction') {
		include('modules/discovery-attraction.php');
	}
	elseif ($pieces[1] == 'guide') {
		include('modules/discovery-guide.php');
	}
	elseif ($pieces[1] == 'article') {
		include('modules/discovery-article.php');
	}
	elseif ($pieces[1] == 'article-detail') {
		include('modules/discovery-article-detail.php');
	}
?>
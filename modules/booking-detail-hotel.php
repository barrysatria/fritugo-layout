<a href="?page=user/booking" class="UserBack">< &nbsp; &nbsp;Back to My Booking</a>
<div class="BookingDetail">
	<div class="BDCaption">
		Booking Details
	</div>
	<hr>
	<div class="row">
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking by
			</div>
			<div class="BDOutput">
				Jon Snow
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking Date
			</div>
			<div class="BDOutput">
				17 April 2018
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking ID
			</div>
			<div class="BDOutput">
				78346334
			</div>
		</div>
	</div>
	<hr>
	<div class="BDCaption">
		PAYMENT
	</div>
	<div class="row">
		<div class="col-xs-4">
			<div class="BDLabel">
				Payment Methods
			</div>
			<div class="BDOutput">
				Transfer
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Payment Status
			</div>
			<div class="BDOutput">
				<button class="BDTicket">TICKET ISSUED</button>
			</div>
		</div>
	</div>
	<div>
		<hr class="BDHRDashed">
	</div>
	<div>
		<button class="BDReceipt">Receipt</button>
	</div>
</div>
<div class="BookingDetail">
	<div class="BDCaption">
		Hotel Details
	</div>
	<hr>
	<div class="BDHotel">
		<div class="col-xs-2">
			<img src="assets/images/novotel-room.jpg">
		</div>
		<div class="col-xs-7">Novotel Bogor Golf Resort & Convention Center</div>
	</div>
	<div class="clearfix">
		
	</div>
	<div class="BDHotel">
		<div class="col-xs-4">
			Check-in
		</div>
		<div class="col-xs-4">
			Wed, 28 Jun 2018
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-4">
			Check-out
		</div>
		<div class="col-xs-4">
			Thu, 29 Jun 2018
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-4">
			Room type
		</div>
		<div class="col-xs-4">
			Superior room, 2 twin beds
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-4">
			Booking code
		</div>
		<div class="col-xs-4">
			734636434
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-4">
			Specific request
		</div>
		<div class="col-xs-4">
			-
		</div>
		<div class="clearfix"></div>

	</div>
	
	
</div>
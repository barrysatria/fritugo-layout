<div class="FBookingSection">
	<div class="FBSBreadcrumb">
		<div class="numberCircle">1</div><div>Booking</div><div class="FBSBreadcrumbLine"></div>
		<div class="numberCircle">2</div><div>Review</div><div class="FBSBreadcrumbLine"></div>
		<div class="numberCircle">3</div><div>Payment</div><div class="FBSBreadcrumbLine"></div>
		<div class="numberCircle">4</div><div>Process</div><div class="FBSBreadcrumbLine"></div>
		<div class="numberCircle">5</div><div>E-ticket issued</div>
	</div>
</div>
<?php 
	if ($pieces[1] == 'booking-information') {
		include('modules/hotel-booking-information.php');
	}
	if ($pieces[1] == 'booking-review') {
		include('modules/hotel-booking-review.php');
	}
?>
<div class="Discovery">
	<div class="DBreadcrumb">
		<div>Discovery / Asia / Indonesia / Attractions</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DCHAttraction">
		<p>Attractions</p>
		<div class="row">
			<div class="col-xs-9">
				<hr>
				<div class="DCHAItem row">
					<div class="col-xs-9"> 
						<p><span>TOP CHOICE</span> MUSEUM IN DENPASAR</p>
						<p>Museum Negeri Provinsi Bali</p>
						<p>Think of this as British Museum or the Smithsonian of Balinese culture. It's all here, but unlike those world-class institutions. You have to work at sorting it out - the museum could use a dose of curatorial en...</p>
					</div>
					<div class="col-xs-3">
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
				<hr>
				<div class="DCHAItem row">
					<div class="col-xs-9"> 
						<p><span>TOP CHOICE</span> MUSEUM IN DENPASAR</p>
						<p>Museum Negeri Provinsi Bali</p>
						<p>Think of this as British Museum or the Smithsonian of Balinese culture. It's all here, but unlike those world-class institutions. You have to work at sorting it out - the museum could use a dose of curatorial en...</p>
					</div>
					<div class="col-xs-3">
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
				<hr>
				<div class="DCHAItem row">
					<div class="col-xs-9"> 
						<p><span>TOP CHOICE</span> MUSEUM IN DENPASAR</p>
						<p>Museum Negeri Provinsi Bali</p>
						<p>Think of this as British Museum or the Smithsonian of Balinese culture. It's all here, but unlike those world-class institutions. You have to work at sorting it out - the museum could use a dose of curatorial en...</p>
					</div>
					<div class="col-xs-3">
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
				<hr>
				<div class="DCHAItem row">
					<div class="col-xs-9"> 
						<p><span>TOP CHOICE</span> MUSEUM IN DENPASAR</p>
						<p>Museum Negeri Provinsi Bali</p>
						<p>Think of this as British Museum or the Smithsonian of Balinese culture. It's all here, but unlike those world-class institutions. You have to work at sorting it out - the museum could use a dose of curatorial en...</p>
					</div>
					<div class="col-xs-3">
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
				<hr>
				<div class="DCHAItem row">
					<div class="col-xs-9"> 
						<p><span>TOP CHOICE</span> MUSEUM IN DENPASAR</p>
						<p>Museum Negeri Provinsi Bali</p>
						<p>Think of this as British Museum or the Smithsonian of Balinese culture. It's all here, but unlike those world-class institutions. You have to work at sorting it out - the museum could use a dose of curatorial en...</p>
					</div>
					<div class="col-xs-3">
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<hr>
				<div>
					<p>TYPE</p>
					<div class="col-xs-6">
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Museum</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Hindu Temple</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Mosque</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Market</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Palace</label>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Beach</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Village</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Historic Building</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Temple</label>
						</div>
						<div class="checkbox">
						  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Monument</label>
						</div>
					</div>
					<a>MORE CATEGORIES ></a>
				</div>
			</div>
		</div>
	</div>
</div>

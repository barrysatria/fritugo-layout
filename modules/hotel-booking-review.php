<div class="row FBInformation">
	<div class="col-sm-12">
		<h3>Please Review Your Booking</h3>
		
		<h4>Your Information</h4>
	</div>
	<div class="clearfix">
	</div>
	
	<div class="col-sm-8">
		<div class="FBILeft">
			<div class="row">
				<div class="col-sm-3">
					<img class="HBRImg" src="assets/images/afgan.jpg">
				</div>
				<div class="col-sm-9">
					<p class="HBRCaption">Novotel Nusa Dua</p>
					<div class="row">
						<div class="col-xs-4">
							<p class="HBRP1">Check-in</p>
							<p class="HBRP2">Thu, 1 Sep 2018</p>
						</div>
						<div class="col-xs-4">
							<p class="HBRP1">Check-in</p>
							<p class="HBRP2">Sun, 4 Sep 2018</p>
						</div>
						<div class="col-xs-4">
							<p class="HBRP1">Duration of Stay</p>
							<p class="HBRP2">2 Nights</p>
						</div>
						<div class="col-xs-4">
							<p class="HBRP1">Guest Name</p>
							<p class="HBRP2">Brian Wangsa</p>
						</div>
					</div>
					<hr class="HBRHr" />
					<p class="HBRP1">Room Detail</p>
					<div class="row HBRP2">
						<div class="col-xs-4">
							Room type
						</div>
						<div class="col-xs-8">
							typeRoyal Suite Ocean Court - Domestic & Kitas Holder Only
						</div>
						<div class="col-xs-4">
							No. of Room
						</div>
						<div class="col-xs-8">
							1 room
						</div>
						<div class="col-xs-4">
							Guest per room
						</div>
						<div class="col-xs-8">
							2 Guests
						</div>
						<div class="col-xs-4">
							Special Request
						</div>
						<div class="col-xs-8">
							-
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="FBILeft">
			<p class="HBPSpecial">Cancellation policy</p>
			<p class="HBPExplain">This reservation is not refundable. Time displayed are based on the hotel's local time. Time priode and room type are non-changeable.</p>
		</div>
		<h4>Price Details</h4>
		<div class="FBILeft">
			<p class="HBPSpecial">Mulia resort</p>
			<div class="row HBPrice">
				<div class="col-xs-8">
					Royal Suite Ocean Court - Domestic & Kitas Holder Only (2 Night) x1
				</div>
				<div class="col-xs-4">
					Rp. 8.754.050
				</div>
				<div class="col-xs-8">
					Convenience Fee
				</div>
				<div class="col-xs-4">
					<span>Rp. -427</span>
				</div>
				<div class="col-xs-8">
					Taxt Recovery Charges
				</div>
				<div class="col-xs-4">
					Included
				</div>
				<div class="col-xs-8">
					Fritugo Fee
				</div>
				<div class="col-xs-4">
					<span>FREE</span>
				</div>
			</div>
			<hr>
			<div class="row HBTotalPrice">
				<div class="col-xs-8">
					Total
				</div>
				<div class="col-xs-4">
					Rp. 10.591.973
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				By clicking the button, you agree to Fritugo's<br>
				<a href="#" title="">Terms & Conditions</a> and <a href="#" title="">Privacy Policy</a>
			</div>
			<div class="col-md-4">
				<button onclick="window.location='?page=hotel-booking/booking-review';" type="submit" class="btn btn-warning pull-right">Continue To Payment</button>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="HBRRightBottom">
			<h3>Continue to payment</h3>
			<p>By clicking the button below, you agree to Fritugo's <a href="#" title="">Terms & Conditions</a> and <a href="#" title="">Privacy Policy</a></p>
			<button type="submit" class="btn btn-warning">Continue To Payment</button>
		</div>
		
		
	</div>
</div>
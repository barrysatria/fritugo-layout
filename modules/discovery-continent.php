<div class="Discovery">
	<div class="DBreadcrumb">
		<div>Discovery / Asia / Indonesia</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DCHContinentSection1">
		<p>INDONESIA</p>
		<h3>Is Culturally Rich</h3>
	</div>
	<div class="DCHContinentSection2">
		<center>
			<span>EXPERIENCES</span>
			<span>SURVIVAL GUIDE</span>
			<span>INTERESTS</span>
			<span>ITINERARIES</span>
			<h1>Why I love Indonesia</h1>
			<div></div>
			<p>“I was driving across beautiful Flores and rounding a corner somewhere east of the steamy port town of Ende, when the most brilliant view of a volcano filled my windscreen. Conical, with obvious recent lava flows down the side and a little ominous wisp of steam rising from the top. And I thought: 'That's the third one today!' And so it goes in Indonesia – where the natural beauty is as diverse as the people who live there...” <a href="?page=discovery/article-detail">READ MORE</a></p>
			<img src="assets/images/profile-photo.jpg" alt="">
			<p class="DCHCS2Name">RYAN VER BERKMOES</p>
			<p class="DCHCS2Work">Writer</p>
		</center>
		<hr>
		<center>
			<h1>Top Experiences in Indonesia</h1>
			<div></div>
		</center>
		<div class="row">
			<div class="col-sm-3">
				<div style="background-image: url('assets/images/komodo.jpg');" onclick="window.location='?page=discovery/detail';"><p class="DCHCS2ExpPlace">Komodo & Rinca Island</p>
				<p class="DCHCS2Exp">KOMODO NATIONAL PARK</p></div>
				
			</div>
			<div class="col-sm-3">
				<div style="background-image: url('assets/images/gili.jpg');"  onclick="window.location='?page=discovery/detail';">
				<p>Gili Islands</p>
				<p>GILI ISLANDS</p></div>
			</div>
			<div class="col-sm-3">
				<div style="background-image: url('assets/images/bali.jpg');"  onclick="window.location='?page=discovery/detail';">
				<p>Central Highland</p>
				<p>BALINESE DANCE</p></div>
			</div>
			<div class="col-sm-3">
				<div style="background-image: url('assets/images/kapuas.jpg');" onclick="window.location='?page=discovery/detail';">
				<p>Sungai Kapuas</p>
				<p>KAPUAS</p></div>
			</div>
		</div>
		<div class="DCHCS2TopSight">
			<p>Top Sight in Indonesia</p>
			<div class="row">
				<div class="col-md-8">
					<div class="col-xs-6">
						<table>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Museum Negeri Provinsi Bali</p> <p>DENPASAR</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Goa Gajah</p> <p>BEDULU</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Neka Art Museum</p> <p>UBUD</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Merdeka Square</p> <p>MERDEKA SQUARE & CENTRAL JAKARTA</p></td>
							</tr>
						</table>
						
					</div>
					<div class="col-xs-6">
						<table>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Gunung Kawi</p> <p>TAMPAKSIRING</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Museum Nasional</p> <p>MERDEKA SQUARE & CENTRAL JAKARTA</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Puri Agung Karangasem</p> <p>AMLAPURA</p></td>
							</tr>
							<tr>
								<td><img src="assets/images/kapuas.jpg" alt=""></td>
								<td><p>Pura Luhur Ulu Watu</p> <p>ULU WATU & AROUND</p></td>
							</tr>
						</table>
						
					</div>
				</div>
				<div class="col-md-4">
					<div>
						<p>ADVERTISMENT</p>
						<img src="assets/images/bali.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<hr>
		<center>
			<h1>Survival Guide</h1>
			<div></div>
		</center>
		<div class="DCHCS2SurvivalGuide row">
			<div class="col-sm-3">
				<p><i class="fa fa-address-card" aria-hidden="true"></i></p>
				<p>Visas</p>
				<p>Dull but essenstial passpor paperwork and entery info</p>
			</div>
			<div class="col-sm-3">
				<p><i class="fa fa-umbrella" aria-hidden="true"></i></p>
				<p>Best time to go</p>
				<p>Hit the ground at the right time</p>
			</div>
			<div class="col-sm-3">
				<p><i class="fa fa-money" aria-hidden="true"></i></p>
				<p>Money and costs</p>
				<p>Budgets, currency rate and on-the-ground costs</p>
			</div>
			<div class="col-sm-3">
				<p><i class="fa fa-stethoscope" aria-hidden="true"></i></p>
				<p>Health</p>
				<p>Keep safe and well on the open road</p>
			</div>
		</div>
		<div class="DCHCS2SeeMore">
			<a href="?page=discovery/guide">SEE MORE ></a>
		</div>
	</div>
</div>

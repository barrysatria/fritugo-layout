<div class="Discovery">
	<div class="DBreadcrumb">
		<div>Discovery / Asia / Indonesia / Komodo Rinca Island</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DCHDetail">
		<center>
			<p>Komodo National Park</p>
			<p>Wildlife Reserve in Komodo & Rinca Island</p>
		</center>
		<div class="row">
			<div class="col-xs-9">
				<img src="assets/images/bali.jpg" alt="">
				<span>Image by Raimundo Fernandez</span>
				<div class="DCHDArticle">
					<p>Declared one of the New Seven Wonders (www.n7w.com) of nature, this national park, established in 1980, encompasses Komodo, Rinca, several neighbouring islands, and the rich marine ecosystem within its 1817 sq km.</p>
					<p>A three-day visitor permit includes your park entrance fee (50,000Rp) and the conservation fee (20,000Rp), collected on arrival by rangers. Camera (50,000Rp), video camera (150,000Rp), snorkelling (60,000Rp) and diving (75,000Rp) taxes apply, as well. All fees are good for three days.</p>
					<div class="DCHDAds row">
						<div class="col-xs-6">
							<img src="assets/images/komodo.jpg" alt="">
						</div>
						<div class="col-xs-6">
							<p>TOUR AND SIGHTSEEING ACTIVITY</p>
							<p>Komodo National Park Tour From Labuan Bajo</p>
							<p>$108</p>
							<a href="#" title="">LEARN MORE ></a>
						</div>
					</div>
					<p>A short, guided dragon-spotting trek is included with your entrance fee. For a longer, hour-long trek on Rinca you’ll pay an additional 50,000Rp. On Komodo, where the hiking is superb, you can pay from 50,000Rp to 250,000Rp for guided treks that range from flat 3km strolls to steep 10km hikes over peaks and into deep valleys. Arrange your trek upon registration in Komodo. All guides speak some English and are very knowledgeable about the islands’ flora and fauna.</p>
				</div>
				<div class="DCHDItin">
					<span>RELATED ITINERARY</span><span class="pull-right">VIEW ALL ITINERARIES</span>
					<hr>
					<img src="assets/images/bali.jpg" alt="">
					<p class="DCHDItinP1">Classic Bali</p>
					<p class="DCHDItinP2">$849</p>
					<p class="DCHDItinP3">8 DAYS - SANUR TO CANDIDASA</p>
				</div>
			</div>
			<div class="col-xs-3">
				<hr>
				<p>CONTACT</p>
				<a href="#" title="">http://komodonationalpark.org</a>
			</div>
		</div>
	</div>
</div>

<p class="UCCaption">List of Travelers</p>
<p>You can add up-to 20 traveler</p>
<div class="UCInner">
	<div class="pull-right negative-button">Change</div>
	<div class="pull-right negative-button">Edit</div>
	<p class="UCTName"><i class="fa fa-user" aria-hidden="true"></i> 1. Mr. Jhon Snow</p>
</div>
<div class="UCInner">
	<p class="UCCaption">Traveler Detail</p>
	<div class="form-inline">
    	<div class="input-group-lg form-group">
	        <label>Title</label><br>
	        <select class="form-control" name="title">
	        	<option value="mr">Mr</option>
	        	<option value="mrs">Mrs</option>
	        </select>
	    </div>
	    <div class="input-group-lg form-group">
	        <label>First Name</label><br>
	        <input type="text" class="form-control" name="firstname" placeholder="First Name">
	    </div>
	    <div class="input-group-lg form-group">
	        <label>Last Name</label><br>
	        <input type="text" class="form-control" name="lastname" placeholder="Last Name">
	    </div>
    </div>
    <div class="row UCTDiv">
    	<div class="col-md-6">
	    	<div class="input-group-lg form-group">
		        <label>Email</label><br>
		        <input type="email" class="form-control" name="email" placeholder="Email">
		    </div>
	    </div>
	    <div class="col-md-6">
	    	<div class="form-inline">
			    <div class="input-group-lg form-group">
			        <label>Phone</label><br>
			        <input type="text" class="form-control UCTPhoneFirst" name="phonefirst" placeholder="+62">
			        <input type="text" class="form-control" name="phonelast" placeholder="872322..">
			    </div>
		    </div>
	    </div>
    </div>
    <div class="row UCTDiv">
    	<div class="col-md-6">
	    	<div class="input-group-lg form-group">
		        <label>Nationalities</label><br>
		        <select class="form-control" name="nation">
		        	<option value="indonesia">Indonesia</option>
		        	<option value="malaysia">Malaysia</option>
		        </select>
		    </div>
	    </div>
	    <div class="col-md-6">
	    	<div class="form-inline">
			    <div class="input-group-lg form-group">
			        <label>Date of Birth</label><br>
			        <select class="form-control" name="birthdate">
			        	<option value="1">1</option>
			        	<option value="2">2</option>
			        	<option value="3">3</option>
			        	<option value="4">4</option>
			        	<option value="5">5</option>
			        </select>
			        <select class="form-control" name="birthmonth">
			        	<option value="1">January</option>
			        	<option value="2">February</option>
			        	<option value="3">March</option>
			        	<option value="4">April</option>
			        	<option value="5">May</option>
			        </select>
			        <select class="form-control" name="birthyear">
			        	<option value="2017">2017</option>
			        	<option value="2016">2016</option>
			        	<option value="2015">2015</option>
			        	<option value="2014">2014</option>
			        	<option value="2013">2013</option>
			        </select>
			    </div>
		    </div>
	    </div>
    </div>
    <hr>
    <div class="row UCTDiv">
    	<div class="col-md-6">
	    	<div class="input-group-lg form-group">
		        <label>Passport Number</label><br>
		        <input type="number" class="form-control" name="number" placeholder="Passport Number">
		    </div>
	    </div>
	    <div class="col-md-6">
	    	<div class="form-inline">
			    <div class="input-group-lg form-group">
			        <label>Passport expire date</label><br>
			        <select class="form-control" name="birthdate">
			        	<option value="1">1</option>
			        	<option value="2">2</option>
			        	<option value="3">3</option>
			        	<option value="4">4</option>
			        	<option value="5">5</option>
			        </select>
			        <select class="form-control" name="birthmonth">
			        	<option value="1">January</option>
			        	<option value="2">February</option>
			        	<option value="3">March</option>
			        	<option value="4">April</option>
			        	<option value="5">May</option>
			        </select>
			        <select class="form-control" name="birthyear">
			        	<option value="2017">2017</option>
			        	<option value="2016">2016</option>
			        	<option value="2015">2015</option>
			        	<option value="2014">2014</option>
			        	<option value="2013">2013</option>
			        </select>
			    </div>
		    </div>
	    </div>
    </div>
    <div class="input-group-lg">
        <label>Country of issue</label><br>
        <select class="form-control" name="nation">
        	<option value="indonesia">Indonesia</option>
        	<option value="malaysia">Malaysia</option>
        </select>
    </div>
    <hr>
    <div class="UCTAddPhoto">+ Identity Card</div>
    <hr>
    <div class="UCTAddPhoto">+ Driving License</div>
    <hr>
    <div class="UCTAddPhoto">+ Other Official Documents</div>
    <hr>
    <button class="pull-right UCIAddButton">Save</button>
    <button class="pull-right UCICancelButton">Cancel</button>
    <div class="clearfix"></div>
</div>
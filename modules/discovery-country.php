<div class="Discovery">
	<div class="DBreadcrumb DBHome">
		<div><span class="glyphicon glyphicon-globe"></span> Explore The World</div>
		<div class="pull-right">
			<a href="#" class="active">All destinations</a>
			<a href="?page=discovery/article">Tips & articles</a>
		</div>
		
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DCHomeAll row">
		<div class="col-sm-2">
			<p>DESTINATIONS</p>
			<hr>
			<div>Africa</div>
			<hr>
			<div class="active">Asia</div>
			<hr>
			<div>Caribbean</div>
			<hr>
			<div>Central America</div>
			<hr>
			<div>Europe</div>
			<hr>
			<div>Middle East</div>
			<hr>
			<div>North America</div>
			<hr>
			<div>Pasific</div>
			<hr>
			<div>South America</div>

		</div>
		<div class="col-sm-10">
			<div class="DCHCaption">Destinations in Asia</div>
			<div class="DCHAItem">
				<div class="DCHAlphabet col-md-12">A</div>
				<div class="col-md-6 DCHCountry"><a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> Afghanistan</a></div>
			</div>
			<div class="DCHAItem">
				<div class="DCHAlphabet col-md-12">B</div>
				<div>
					
					<div class="col-md-6">
						<div class="DCHCountry">
							<a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> Bhutan</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="DCHCountry">
							<a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> Bangladesh</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="DCHCountry">
							<a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> Brunei Darussalam</a>
						</div>
					</div>
				</div>
			</div>
			<div class="DCHAItem">
				<div class="DCHAlphabet col-md-12">C</div>
				<div>
					
					<div class="col-md-6">
						<div class="DCHCountry">
							<a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> Cambodia</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="DCHCountry">
							<a href="?page=discovery/continent" title=""><img src="assets/images/afgan.jpg" alt=""> China</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

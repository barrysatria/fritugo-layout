<?php include('modules/trip-planner-header.php'); ?>
<div class="tripPlannerDetail">
	<button class="TPDButtonBack">Back to Results</button>
	<div class="TDPDesc row">
		<p><b>Bali,</b> Indonesia</p>
		<p>1 Person &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| 01 September 2018 - 04 September 2018</p>
	</div>
	<button class="TPDButtonPurchase"><p>Rp. 5,700,000</p><p><b>Purchase Now</b></p></button>
</div>
<script type="text/babel" src="react_modules/trip-planner-detail.jsx"></script>
<div id="TPD2">
	
</div>
<div class="TPD3">
	<h3>Accommodations</h3>
	<ul class="nav nav-tabs pull-right">
		<li class="active"><a data-toggle="tab" href="#bestDeal">BEST DEAL</a></li>
		<li><a data-toggle="tab" href="#cheapest">CHEAPEST</a></li>
	</ul>
	<div class="clearfix">
	</div>
	<div class="tab-content">
		<div  id="bestDeal" class="tab-pane fade in active">
			<div class="row">
				<div class="col-md-7">
					<img src="assets/images/novotel.jpg" alt="">
					<p>The mid-scale Novotel Bali Benoa Hotel is suitable for a family vacation or romantic gateway in tropical splendor. The resort offers 3 swimming pools, 3 restaurants and bars, fitness centre, spa, kids club, tennis court, well-equipped business centre and free WiFi throughout the hotel.</p>
					<p>Novotel Bali Benoa is located in the popular water sport destination of Tanjung Benoa. The hotel is a beautiful beachside retreat with an incomparable view of the beach, providing guests with easy access to numerous beach and water activities.</p>
				</div>
				<div class="col-md-5 TPD3DescMore">
					<p>Novotel Bali</p>
					<p>
						<span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>
						<span class="pull-right">Rp. 1,500,000</span>
					</p>
					<div class="row row-eq-height">
						<div class="col-xs-5">
							<img src="assets/images/novotel-room.jpg" alt="">
						</div>
						<div class="col-xs-7">
							<p>Double Room with Balcony</p>
							<p>Single Occupancy</p>
							<a>See more rooms</a>
						</div>
					</div>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#bestDeal-facilities" aria-controls="bestDeal-facilities" role="tab" data-toggle="tab"><span>Hotel Facilities</span></a></li>
						<li><a href="#bestDeal-policies" aria-controls="bestDeal-policies" role="tab" data-toggle="tab"><span>Policies</span></a></li>
						<li><a href="#bestDeal-review" aria-controls="bestDeal-review" role="tab" data-toggle="tab"><span>Verified Reviews</span></a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active facilities" id="bestDeal-facilities">
							<table>
								<tr>
									<td><div><i class="fa fa-diamond"></i> </div> <b>General</b></td>
									<td>Pets allowed, Private parking</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bicycle"></i> </div> <b>Activities</b></td>
									<td>Beach</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-rss"></i> </div> <b>Services</b></td>
									<td>Free WIFI</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-cutlery"></i> </div> <b>Food & Drink</b></td>
									<td>Restaurant, room service, Bar, Breakfast in the room, Restaurant(a la carte), Restaurant(buffet)</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bath"></i> </div> <b>Pool & Wellness</b></td>
									<td>Sauna, Spa, Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="bestDeal-policies">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
						<div role="tabpanel" class="tab-pane" id="bestDeal-review">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
					</div>
				</div>
			</div>
		</div>
		<div id="cheapest" class="tab-pane fade">
			<div class="row">
				<div class="col-md-7">
					<img src="assets/images/novotel.jpg" alt="">
					<p>The mid-scale Novotel Bali Benoa Hotel is suitable for a family vacation or romantic gateway in tropical splendor. The resort offers 3 swimming pools, 3 restaurants and bars, fitness centre, spa, kids club, tennis court, well-equipped business centre and free WiFi throughout the hotel.</p>
					<p>Novotel Bali Benoa is located in the popular water sport destination of Tanjung Benoa. The hotel is a beautiful beachside retreat with an incomparable view of the beach, providing guests with easy access to numerous beach and water activities.</p>
				</div>
				<div class="col-md-5 TPD3DescMore">
					<p>Novotel Bali</p>
					<p>
						<span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>
						<span class="pull-right">Rp. 1,500,000</span>
					</p>
					<div class="row row-eq-height">
						<div class="col-xs-5">
							<img src="assets/images/novotel-room.jpg" alt="">
						</div>
						<div class="col-xs-7">
							<p>Double Room with Balcony</p>
							<p>Single Occupancy</p>
							<a>See more rooms</a>
						</div>
					</div>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#cheapest-facilities" aria-controls="cheapest-facilities" role="tab" data-toggle="tab"><span>Hotel Facilities</span></a></li>
						<li><a href="#cheapest-policies" aria-controls="cheapest-policies" role="tab" data-toggle="tab"><span>Policies</span></a></li>
						<li><a href="#cheapest-review" aria-controls="cheapest-review" role="tab" data-toggle="tab"><span>Verified Reviews</span></a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active facilities" id="cheapest-facilities">
							<table>
								<tr>
									<td><div><i class="fa fa-diamond"></i> </div> <b>General</b></td>
									<td>Pets allowed, Private parking</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bicycle"></i> </div> <b>Activities</b></td>
									<td>Beach</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-rss"></i> </div> <b>Services</b></td>
									<td>Free WIFI</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-cutlery"></i> </div> <b>Food & Drink</b></td>
									<td>Restaurant, room service, Bar, Breakfast in the room, Restaurant(a la carte), Restaurant(buffet)</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bath"></i> </div> <b>Pool & Wellness</b></td>
									<td>Sauna, Spa, Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
								<tr>
									<td><div><i class="fa fa-bus"></i> </div> <b>Transportation</b></td>
									<td>Car hire, Shuttle service(charge), Wellness center, Massage, Hammam, Indor Pool(all year), beach front, swimming pool</td>
								</tr>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="cheapest-policies">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
						<div role="tabpanel" class="tab-pane" id="cheapest-review">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="TPD4">
	<h3>Flights</h3>
	<ul class="nav nav-tabs pull-right">
		<li class="active"><a data-toggle="tab" href="#bestDealFlight">BEST DEAL</a></li>
		<li><a data-toggle="tab" href="#cheapestFlight">CHEAPEST</a></li>
	</ul>
	<div class="clearfix">
	</div>
	<div class="tab-content">
		<div  id="bestDealFlight" class="tab-pane fade in active">
			<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
			<div><span>Jakarta - Denpasar, 01 September 2018</span><span><i class="glyphicon glyphicon-briefcase"></i> 20Kg</span><span class="pull-right">Rp. 2,100,000</span></div>
			<div class="TPD4Hr">
				<div></div><div class="pull-right"></div><hr>
			</div>
			<div class="TDP4Arrive">
				<span>CGK, 18.00 WIB</span><span class="pull-right">DPS, 19.52 WIT</span>
			</div>
			<div><span>Denpasar - Jakarta, 04 September 2018</span><span><i class="glyphicon glyphicon-briefcase"></i> 20Kg</span><span class="pull-right">Rp. 2,100,000</span></div>
			<div class="TPD4Hr">
				<div></div><div class="pull-right"></div><hr>
			</div>
			<div class="TDP4Arrive">
				<span>DPS, 18.00 WIB</span><span class="pull-right">CGK, 19.52 WIT</span>
			</div>
		</div>
		<div id="cheapestFlight" class="tab-pane fade">
			<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
			<div><span>Jakarta - Denpasar, 01 September 2018</span><span><i class="glyphicon glyphicon-briefcase"></i> 20Kg</span><span class="pull-right">Rp. 2,100,000</span></div>
			<div class="TPD4Hr">
				<div></div><div class="pull-right"></div><hr>
			</div>
			<div class="TDP4Arrive">
				<span>CGK, 18.00 WIB</span><span class="pull-right">DPS, 19.52 WIT</span>
			</div>
			<div><span>Denpasar - Jakarta, 04 September 2018</span><span><i class="glyphicon glyphicon-briefcase"></i> 20Kg</span><span class="pull-right">Rp. 2,100,000</span></div>
			<div class="TPD4Hr">
				<div></div><div class="pull-right"></div><hr>
			</div>
			<div class="TDP4Arrive">
				<span>DPS, 18.00 WIB</span><span class="pull-right">CGK, 19.52 WIT</span>
			</div>
		</div>
	</div>
</div>
<iframe class="TPD5" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD7PszmE-siLglBQpbu2t5IvpmaVBGGkXQ&q=Novotel+Nusa+Dua+Bali" frameborder="0" allowfullscreen></iframe>
<div class="TPD6">
	<h3><b>Total: Rp. 5,700,000</b></h3>
	<button class="TPD6BSave">Save</button><button class="TPD6BContinue">Continue to checkout</button>
</div>
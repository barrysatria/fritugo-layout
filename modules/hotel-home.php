<div  class="FSection1">
    <div class="FS1P">
        <p class="FS1P1">FIND YOUR BEST HOTEL!</p>
        <p class="FS1P2">Book Hotel deals for more than 1000 room selections</p>
    </div>
    <div class="FS1F">
    	<p><b>Fritugo Global Hotel Search</b></p>
    	<div class="FS1FI row">
            <div class="col-sm-7">
                <label>Destination / Hotel Name</label>
                <div class="form-group input-group-lg">
                    <input type="text" class="form-control" placeholder="city, hotel, place to go" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group input-group-lg">
                    <label for="depart">Check-In</label>
                    <input type="text" class="form-control" id="depart" name="depart" placeholder="depart">
                    <span class="input-group-btn" style="width:0px;"></span>
                    <label for="return">Check-Out</label>
                    <input type="text" class="form-control" id="return" name="return" placeholder="return">
                </div>
            </div>
            <div class="col-sm-2">
            	<div class="input-group input-group-lg">
                    <label for="depart">Check-In</label>
                    <select class="form-control FS1FIRadiusLeft" name="person">
                        <option selected disabled>Person</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                    <span class="input-group-btn" style="width:0px;"></span>
                    <label for="return">Check-Out</label>
                    <select class="form-control" name="person">
                        <option selected disabled>Person</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
            </div>
        </div>
        <center><button type="button" class="btn btn-warning btn-lg" onclick="window.location='?page=hotel-result';">
            FIND HOTELS 
        </button></center>
    </div>
</div>
<p class="TOSCaption">Terms & Conditions of Service</p>
<p class="TOSSubCaption">Fritugo</p>
<p>We, FRITUGO CORP. (“FRITUGO”) are a revolutionary service, which helps users on the Internet find suitable travels, including air tickets, hotels and other accommodation options, and travel-related services (insurance policies, transfers, guided tours) provided by our partners (the partner details can be found in Our Partners section)</p>
<p>When users (website visitors) find an offering, which is suitable to them, FRITUGO connects them to the relevant partners (OneTwoTrip.com for booking and purchase of air tickets (including other related services; Booking.com for booking of hotels and other accommodation options), so that they (website visitors) be able to execute a booking and/or purchasing directly from the partners.</p>
<p>FRITUGO also allows users to transmit any and all data required in order to complete the booking and/or purchase of the selected travel offering by sending the data to the partners via a secure Internet link rather than sending the users to web pages of the relevant partners. </p>
<p>FRITUGO does no execute a booking or accept a payment or stores any payment information.</p>

<p class="TOSSubCaption">Terms and Conditions</p>
<p>These Terms and Conditions, which may be revised from time to time, shall apply to any and all Services provided online, via electronic mail, by telephone or any mobile device.  By visiting, viewing or using our website and/or making a Booking and/or purchase, you acknowledge that you have read, understood and agreed to the below terms and conditions (including the Privacy Policy).</p>

<p>You will be deemed to have accepted any revisions, if You continue using this Website or the Applications after such revisions are published.</p>

<p>These terms and conditions constitute a legal agreement between you and FRITUGO Company CORP. (“the Agreement” or “this Agreement”).</p>

<p>Please carefully read and understand the Agreement before use of this Website or the Applications.  By downloading, accessing, or using this Website or the Applications in any manner whatsoever, You acknowledge that You have read and understood these Terms and Conditions and agree to comply with them.</p>

<p>If You disagree to comply with these Terms and Conditions or any revisions thereto, please stop using this Website and the Applications immediately.</p>
<p class="TOSSubCaption">Booking and/or Purchasing via FRITUGOservice</p>
<p>Booking and/or Purchasing service allows you to book and pay for travel offerings of third-party travel companies, which are our partners (“Our Partners”) selected by you on www.FRITUGO.com website, and is provided, managed and controlled by the relevant Booking Partner, rather than FRITUGO Company.  Your use of the Booking Services, and collection, processing and storage of any personal data (including payment details) provided by you within the framework of this service, shall be governed by the Terms of Service and Privacy Policy of the relevant Booking Partners, information about which will be provided to you via the booking services.  You acknowledge and agree to that in the event if You have any problem with your transaction or booking (purchase) executed by such transaction, you will have to solve this problem with the Booking and/or Purchasing Partner and that Your exclusive legal remedy, including any refund, shall apply to the Booking Partner rather than FRITUGO Company.</p>
<p class="TOSSubCaption">Use of the Website and the Applications</p>
<p>FRITUGO Company manages and controls this Website and the Applications and provides them to you at its own exclusive discretion.
</p>
<p>In consideration of your consent to comply with these Terms and Conditions provision of the Services, FRITUGO will grant you non-transferable non-exclusive license to download, access and use this Website and/or the Applications (including the unique online product of FRITUGO Company which allows searching for travel offerings, including accommodation prices (hotels, apartments, hostels and other accommodation options), flights, guided tours, transfers, insurance policies and other travel products) exclusively for personal non-commercial purposes.</p>

<p>FRITUGO Company shall be entitled to withdraw or modify this Website or the Applications at own discretion at any time, including, without limitation, suspension of access to this Website or the Applications, adding advertisements to this Website or the Applications and limitation of the number of travel offerings for hotels, flights, guided tours, transfers or other travel Services, which may be found via this Website.</p>

<p>In consideration of the permit to use this Website and/or the Applications, you agree to comply with these Terms and Conditions.  Any other use of this Website and the Applications, which is not stipulated by these Terms and Conditions, is prohibited.</p>
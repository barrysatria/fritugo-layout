<div class="Discovery">
	<div class="DBreadcrumb">
		<div>Discovery / Asia / Indonesia / Survival Guide</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DCHGuide">
		<p>Essential Information</p>
		<div class="row">
			<div class="col-xs-9">
				<hr>
				<p>At a glance</p>
				<div class="col-xs-3">
					<div class="DCHGGlance">
						<div class="talk-bubble">
						  	<div class="talktext">
						    	<p>'Salam'</p>
						    	<p>(sa-laam)</p>
						  	</div>
						</div>
					</div>
					<p class="DCHGGlanceP1">'HELLO' IN:</p>
					<p class="DCHGGlanceP2">Indonesian</p>
				</div>
				<div class="col-xs-3">
					<div class="DCHGGlance">
						<img src="assets/images/socket.jpg" alt="">
					</div>
					<p class="DCHGGlanceP1">ELECTRICITY:</p>
					<p class="DCHGGlanceP2">230V/50hz</p>
				</div>
				<div class="col-xs-3">
					<div class="DCHGGlance">
						<div class="DCHGGDate">May - Sep</div>
						<img src="assets/images/best-time.jpg" alt="">
					</div>
					<p class="DCHGGlanceP1">BEST TIME TO GO</p>
					<p class="DCHGGlanceP2">Dry</p>
				</div>
				<div class="col-xs-3">
					<div class="DCHGGlance">
						<div class="DCHGGDate">Sep</div>
						<div class="DCHGGPrice"><span>$ </span>15</div>
					</div>
					<p class="DCHGGlanceP1">GETTING THERE</p>
					<p class="DCHGGlanceP2">CGK <span class="fa fa-plane" aria-hidden="true"></span> TKG</p>
					<div class="DCHGMore">More on getting there</div>
				</div>
				<div class="clearfix"></div>
				<hr>
				<p><span class="fa fa-money" aria-hidden="true"> </span> Money and costs</p>
				<p class="DCHGGDailyCost">DAILY COSTS</p>
				<div class="row">
					<div class="col-xs-3">
						<p class="DCHGGDCBudget">BUDGET (up to)</p>
						<p class="DCHGGDCPrice">Rp. 500,000</p>
						<div class="DCHGGDCDiv"></div>
						<ul class="DCHGGDCList">
							<li>Simple room less than Rp 200,000</li>
							<li>Cheap street meal under Rp 20,000</li>
							<li>Travel like lokal through much of Indonesia outside of major cites and tourist area</li>
						</ul>

					</div>
					<div class="col-xs-3">
						<p class="DCHGGDCBudget">MIDRANGE</p>
						<p class="DCHGGDCPrice">Rp. 500,000 -<br> Rp. 2,000,000</p>
						<div class="DCHGGDCDiv2"></div>
						<ul class="DCHGGDCList">
							<li>Simple room less than Rp 200,000</li>
							<li>Cheap street meal under Rp 20,000</li>
							<li>Travel like lokal through much of Indonesia outside of major cites and tourist area</li>
						</ul>

					</div>
					<div class="col-xs-3">
						<p class="DCHGGDCBudget">TOP END (more than)</p>
						<p class="DCHGGDCPrice">Rp. 2,000,000</p>
						<div class="DCHGGDCDiv3"></div>
						<ul class="DCHGGDCList">
							<li>Simple room less than Rp 200,000</li>
							<li>Cheap street meal under Rp 20,000</li>
							<li>Travel like lokal through much of Indonesia outside of major cites and tourist area</li>
						</ul>

					</div>
				</div>
				<div class="clearfix"></div>
				<hr>
				<p><span class="fa fa-book" aria-hidden="true"> </span> Visas</p>
				<p class="DCHGGDCVisaDesc">Visas are the biggest headache many trevellersface in their Indonesian trip. They are not hard to obtain, but the most common - 30 days - is very short for such as big place. Many travellers find even the 60-day....</p>
				<div class="clearfix"></div>
				<hr>
				<p><span class="fa fa-book" aria-hidden="true"> </span> When to go and weather</p>
				<div class="pull-right DCHGGDCBeforeMap">
					<div class="btn-group DCHGGDCRadio" data-toggle="buttons">
						<label class="btn btn-primary active">
							<input type="radio" name="weather" autocomplete="off" checked> &#8451;
						</label>
						<label class="btn btn-primary">
							<input type="radio" name="weather" autocomplete="off"> &#8457;
						</label>
					</div>&nbsp; /&nbsp;
					<div class="btn-group DCHGGDCRadio" data-toggle="buttons">
						<label class="btn btn-primary active">
							<input type="radio" name="distance" autocomplete="off" checked> MM
						</label>
						<label class="btn btn-primary">
							<input type="radio" name="distance" autocomplete="off"> IN
						</label>
					</div>
				</div>
				<div id="map"></div>
				<script>

				// The following example creates complex markers to indicate beaches near
				// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
				// to the base of the flagpole.

				function initMap() {
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 4,
						center: {lat: -2.073764, lng: 117.072319},
						styles: [{
							"featureType": "administrative.neighborhood",
							"elementType": "labels",
							"stylers": [{
								"visibility": "off"
							}]
						}, {
							"featureType": "administrative.land_parcel",
							"elementType": "labels",
							"stylers": [{
								"visibility": "off"
							}]
						}, {
							"featureType": "administrative.country",
							"elementType": "labels",
							"stylers": [{
								"visibility": "off"
							}]
						}, {
							"featureType": "administrative.locality",
							"elementType": "labels",
							"stylers": [{
								"visibility": "off"
							}]
						}]
					});
					setMarkers(map);
				}

				// Data for the markers consisting of a name, a LatLng and a zIndex for the
				// order in which these markers should display on top of each other.
				var beaches = [
				['GO Apr-Sep', -2.2097019,113.8666454, 1],
				['GO Apr-Oct', -6.149726, 106.83946, 2],
				['GO Jul-Sep', -7.803249,110.3398252, 3],
				['GO Jun-aug', -8.5117916, 115.2303087, 4],
				['GO Apr-Des', -4.0845985,137.1778631, 5]
				];

				function setMarkers(map) {
				// Adds markers to the map.

				// Marker sizes are expressed as a Size of X,Y where the origin of the image
				// (0,0) is located in the top left of the image.

				// Origins, anchor positions and coordinates of the marker increase in the X
				// direction to the right and in the Y direction down.
				var image = {
					url: '',
				// This marker is 20 pixels wide by 32 pixels high.
				size: new google.maps.Size(20, 32),
				// The origin for this image is (0, 0).
				origin: new google.maps.Point(0, 0),
				// The anchor for this image is the base of the flagpole at (0, 32).
				anchor: new google.maps.Point(0, 32)
				};
				// Shapes define the clickable region of the icon. The type defines an HTML
				// <area> element 'poly' which traces out a polygon as a series of X,Y points.
				// The final coordinate closes the poly by connecting to the first coordinate.
				var shape = {
					coords: [1, 1, 1, 20, 18, 20, 18, 1],
					type: 'poly'
				};
				for (var i = 0; i < beaches.length; i++) {
					var beach = beaches[i];
					var marker = new google.maps.Marker({
						position: {lat: beach[1], lng: beach[2]},
						map: map,
						icon: image,
						label: beach[0],
						shape: shape,
						title: beach[0],
						zIndex: beach[3]
					});
				}
				}
				</script>
				<div class="clearfix"></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>J</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>F</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>M</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>A</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMShoulder"><span>M</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMShoulder"><span>J</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMHigh"><span>J</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMHigh"><span>A</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMShoulder"><span>S</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>O</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>N</span></div>
				<div class="col-xs-1 DCHGGDCMonth DCHGGDCMLow"><span>D</span></div>
				<div class="clearfix"></div>
				<div class="col-sm-4 DCHGGDCSeason">
					<p class="DCHGGDCSeasonLow"><span></span> Low Season (Oct-Apr)</p>
					<p>Weat season in Java, Bali and Lombok (and Kalimantan Flowers). Dry Season (best of diving) in Maluku and Papua. Easy to find deals and you can travel with little advance booking (except for Christmas and New Year)</p>
				</div>
				<div class="col-sm-4 DCHGGDCSeason">
					<p class="DCHGGDCSeasonShoulder"><span></span> Shoulder (May, Jun & Sep)</p>
					<p>Weat season in Java, Bali and Lombok (and Kalimantan Flowers). Dry Season (best of diving) in Maluku and Papua. Easy to find deals and you can travel with little advance booking (except for Christmas and New Year)</p>
				</div>
				<div class="col-sm-4 DCHGGDCSeason">
					<p class="DCHGGDCSeasonHigh"><span></span> High Season (Jul & Aug)</p>
					<p>Weat season in Java, Bali and Lombok (and Kalimantan Flowers). Dry Season (best of diving) in Maluku and Papua. Easy to find deals and you can travel with little advance booking (except for Christmas and New Year)</p>
				</div>
			</div>
			<div class="col-xs-3">
				<img src="assets/images/right-photo.jpg" alt="">
			</div>
		</div>
	</div>
</div>

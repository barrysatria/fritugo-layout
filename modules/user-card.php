
<p class="UCCaption">My Cards</p>
<div class="UCInner">
	<table>
		<tr>
			<td>
				<label class="switch">
				 	<input type="checkbox">
				  	<span class="slider round"></span>
				</label>
			</td>
			<td>
				<p>Enable CVV Authentication</p>
				<p>You will be prompted to enter the 3-digit CVV each time to perform a transaction.</p>
			</td>
		</tr>
	</table>
</div>
<div class="UCInner row">
	<div class="col-xs-4">
		<table>
			<tr>
				<td><img src="assets/images/visa-blue.jpg" alt=""></td>
				<td>
					<p>Card Number</p>
					<p>••••4100</p>
				</td>
			</tr>
		</table>
		
	</div>
	<div class="col-xs-8">
		<div class="pull-right">Delete
		</div>
		<table>
			<tr>
				<td>
					<p>Valid Until</p>
					<p>06/2018</p>
				</td>
			</tr>
		</table>
		
	</div>
</div>
<div class="UCInner">
	<p>Add new card</p>
	<p>We accept Visa and MasterCard credit cards.</p>
	<div class="UCIGrey">
		<img src="assets/images/secure.png" alt="">
		<img src="assets/images/verified-visa-master.png" class="pull-right" alt="">
	</div>
	<div class="input-group-lg form-group">
        <label for="name">Name on Card</label>
        <input type="text" class="form-control" name="name" placeholder="Name on Card">
    </div>
    <div class="form-inline">
    	<div class="input-group-lg form-group">
	        <label for="name">Card Number</label><br>
	        <input type="number" class="form-control" name="card" placeholder="Card Number">
	    </div>
	    <div class="input-group-lg form-group">
	        <label for="name">Valid Until</label><br>
	        <input type="date" class="form-control" name="valid" placeholder="Valid Until">
	    </div>
	    <div class="input-group-lg form-group">
	        <label for="name">CVV</label><br>
	        <input type="number" class="form-control" name="CVV" placeholder="CVV">
	    </div>
    </div>
    <p class="UCITerm">By clicking button "Add Card", you agree with <a href="#">Terms & Conditions</a> and <a href="#" title="">Privacy Police</a></p>
    <button class="pull-right UCIAddButton"><i class="fa fa-lock" aria-hidden="true"></i> Add Card</button>
    <button class="pull-right UCICancelButton">Cancel</button>
    <div class="clearfix"></div>
</div>
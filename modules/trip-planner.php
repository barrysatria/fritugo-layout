<div class="TPSection1" id="TPSdata1">
	<center>
		<div id="tripPlannerSlide" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active">
					<h1><b>MY BUDGET</b></h1>
					<input type="text" id="TPAmount" class="TPS1Slide1Input" value="500,000">
					<div>
						<div class="TPS1Check">
							<label>
								<input type="checkbox" value="1"><span>Flight</span>
							</label>
						</div>
						<div class="TPS1Check">
							<label>
								<input type="checkbox" value="2"><span>Hotel</span>
							</label>
						</div>
						<div class="TPS1Check">
							<label>
								<input type="checkbox" value="3"><span>Attraction</span>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-offset-1 col-xs-2 text-right">As cheaper as possible</div> 
						<div class="col-xs-6"><input id="tripPlanner" type="text"  data-slider-tooltip="hide"/></div>
						<div class="col-xs-2 text-left">No limit</div>
					</div>
				</div>

				<div class="item">
					<h1><b>WHEN?</b></h1>
					<span class="TPS2Label">Dates: </span>
					<div class="form-group has-feedback inline-block">
					    <input type="text" class="form-control input-lg date" placeholder="Depart"  data-provide="datepicker"/>
					    <i class="form-control-feedback glyphicon glyphicon-calendar"></i>
					</div>
					<span class="TPS2Label"> - </span>
					<div class="form-group has-feedback has-feedback inline-block">
					    <input type="text" class="form-control input-lg date" placeholder="Return"  data-provide="datepicker"/>
					    <i class="form-control-feedback glyphicon glyphicon-calendar"></i>
					</div>
					<span class="TPS2Label TPS2Label2">With: </span>
					<div class="form-group has-feedback TPS2Select t1 inline-block">
					    <select class="form-control  input-lg" name="person">
	                        <option selected disabled>-</option>
	                        <option>1</option>
	                        <option>2</option>
	                        <option>3</option>
	                        <option>4</option>
	                    </select>
					    <i class="form-control-feedback has-feedback-custom fa fa-users"></i>
					</div>
					<div class="form-group has-feedback TPS2Select t2 inline-block">
					    <select class="form-control  input-lg" name="person">
	                        <option selected disabled>-</option>
	                        <option>1</option>
	                        <option>2</option>
	                        <option>3</option>
	                        <option>4</option>
	                    </select>
					    <i class="form-control-feedback  has-feedback-custom  fa fa-child"></i>
					</div>
					<div class="form-group has-feedback TPS2Select t3 inline-block">
					    <select class="form-control  input-lg" name="person">
	                        <option selected disabled>-</option>
	                        <option>1</option>
	                        <option>2</option>
	                        <option>3</option>
	                        <option>4</option>
	                    </select>
					    <i class="form-control-feedback  has-feedback-custom fa fa-cart-plus"></i>
					</div>
					<div class="TPS2Flight">
						<span class="TPS2Label">Flight from: </span><span class="TPS2FLocation"><i class="fa fa-plane" aria-hidden="true"></i> Jakarta, Indonesia</span>
					</div>
				</div>

				<div class="item">
					<h1><b>&nbsp;</b></h1>
					<h1><b>Please wait......?</b></h1>
				</div>
			</div>
			<a class="left carousel-control TPS1SlideButton" href="#tripPlannerSlide" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control TPS1SlideButton" href="#tripPlannerSlide" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</center>
</div>
<div class="TPSection1" id="TPSdata2">
	<h1><b>WHERE DO YOU WANNA GO?</b></h1>
	<center>
		<div class="TPS1List">
			<span class="glyphicon glyphicon-tree-conifer"></span>
			<p>Beach<p>
		</div>
		<div class="TPS1List">
			<span class="glyphicon glyphicon-tent"></span>
			<p>Mountain<p>
		</div>
		<div class="TPS1List">
			<span class="glyphicon glyphicon-home"></span>
			<p>Sightseeing<p>
		</div>
		<div class="TPS1List">
			<span class="glyphicon glyphicon-shopping-cart"></span>
			<p>Shooping<p>
		</div>
		<div class="TPS1List">
			<span class="glyphicon glyphicon-search"></span>
			<p class="TPS1LForce">Spesific Destination<p>
		</div>
	</center>
	<button class="btn btn-warning btn-lg" id="goToSlide">SURPRISE ME!</button>
</div>
<div class="TPSection2">
	<h2>ITINERARIES</h2>
	<div class="row">
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
		<div class="col-sm-4">
			<div class="TPS2Product" style="background-image: url('assets/images/bali.jpg');">
				<p>BALI, INDONESIA</p>
				<p>4 DAY</p>
				<div class="layer">
    			</div>
			</div>
			<p>14 Places</p>
			<p>Rp. 500,000</p>
		</div>
	</div>
	<center>
		<button type="submit" class="btn btn-lg">SEE MORE</button>
	</center>
</div>
<div class="TPSection3">
	<h2>POPULAR DESTINATION</h2>
	<div class="row">
		<div class="col-sm-3">
			<div class="TPS3Product" style="background-image: url('assets/images/bali.jpg');">
				<p>KUTA BEACH, BALI</p>
				<p>INDONESIA</p>
				<div class="layer">
    			</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="TPS3Product" style="background-image: url('assets/images/bali.jpg');">
				<p>KUTA BEACH, BALI</p>
				<p>INDONESIA</p>
				<div class="layer">
    			</div>
			</div>
		</div>		
		<div class="col-sm-3">
			<div class="TPS3Product" style="background-image: url('assets/images/bali.jpg');">
				<p>KUTA BEACH, BALI</p>
				<p>INDONESIA</p>
				<div class="layer">
    			</div>
			</div>
		</div>		
		<div class="col-sm-3">
			<div class="TPS3Product" style="background-image: url('assets/images/bali.jpg');">
				<p>KUTA BEACH, BALI</p>
				<p>INDONESIA</p>
				<div class="layer">
    			</div>
			</div>
		</div>		
	</div>
	<center>
		<button type="submit" class="btn btn-lg">SEE MORE</button>
	</center>
</div>
<div class="TPSection4">
	<h2>HOW IT WORKS</h2>
	<center>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</center>
	
</div>
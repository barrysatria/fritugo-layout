<div class="Intineraries">
	<div class="IBreadcrumb">
		<a>ASIA</a>
		<a>EUROPE</a>
		<a>NORTH AMERICA</a>
		<a>AFRICA</a>
		<a>SOUTH AMERICA</a>
		<a>OCEANIA</a>
	</div>
</div>
<div class="IntinerariesContent">
	<div class="ICInner">
		<span class="ICCaption">Intineraries on Fritugo</span>
		<button class="ICCaptionButton pull-right">Plan a Trip</button>
		<div class="ICSearch row">
			<div class="col-sm-9">
				<div class="form-group has-feedback">
				    <input type="text" class="form-control" placeholder="Enter a City or Country" />
				    <i class="glyphicon glyphicon-search form-control-feedback ICSIcon"></i>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICSInput1">
					<div class="form-group">
						<select class="form-control">
							<option value="">Recent</option>
							<option value="">Most view</option>
							<option value="">Popular</option>
						</select>
					</div>
				</div>
				<div class="ICSInput2">
					<div class="form-group">
						<select class="form-control">
							<option selected disabled>Select Duration</option>
							<option value="">1-5 Days</option>
							<option value="">6-10 Days</option>
							<option value="">11-15 Days</option>
							<option value="">16-20 Days</option>
							<option value="">21-25 Days</option>
							<option value="">26-30 Days</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="ICList row">
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ICItem">
					<div class="ICITop">
						<div class="ICITop1">
							<p>Bandung</p>
						</div>
						<div class="ICITop2">
							<span>3 Days</span><span class="pull-right">2 Views</span>
						</div>
					</div>
					<div class="ICIBottom">
						<div><p><b>Guest User</b></p></div>
						<div>City: <span class="ICIBLeft">Bandung</span><span class="pull-right ICIBRight">Rp. 6,338,425</span></div>
					</div>
				</div>
			</div>
		</div>
		<div class="ICMore">
			<center><a href="#">Load more Itineraries</a></center>
		</div>
	</div>
</div>

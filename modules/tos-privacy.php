<div class="FBookingSection">
	<div class="FBSBreadcrumb">
		<p>PRIVACY POLICY & COOKIES</p>
	</div>
</div>
<div class="TosSection row">
	<div class="col-sm-2">
		<ul class="nav nav-stacked">
			<li><a href="?page=tos-privacy/term" class="<?php echo ($pieces[1] == 'term' ? 'active' : ''); ?>">Terms & Conditions of Service</a></li>
			<li><a href="?page=tos-privacy/privacy" class="<?php echo ($pieces[1] == 'privacy' ? 'active' : ''); ?>">Privacy Police & Cookies</a></li>
		</ul>
	</div>
	<div class="col-sm-10">
		<?php 
			if ($pieces[1] == 'term') {
				include('modules/tos-privacy-term.php');
			}
			if ($pieces[1] == 'privacy') {
				include('modules/tos-privacy-service.php');
			}
		?>
	</div>
</div>

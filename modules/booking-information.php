<div class="row FBInformation">
	<div class="col-sm-12">
		<h3>Booking</h3>
	</div>
	<div class="col-sm-8">
		<div class="FBILeft">
			<p><span class="FBILSpan">Login or Register</span> to enjoy these member-only benefits</p>
			<div><span class="glyphicon glyphicon-user"></span></div>
			<div>
				<p>Travelers picker</p>
				<p>Book faster and easier by using your saved contact and passangers info</p>
			</div>
		</div>
		<h4>Contact Information</h4>
		<div class="FBILeft">
				<div class="FBILInput1 input-group-lg">
                    <label for="name">Contact Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                    <p class="FBILIFoot">As on ID Card/passport/driving licence (without degree or special characters)</p>
                </div>
                <div class="FBILInput2 form-group input-group-lg">
                    <label for="name">Contact's mobile phone number</label>
                    <input type="text" class="form-control" id="name" name="name">
                    <p class="FBILIFoot">e.g. +62812345678, for Country Code (+62) and Mobile No. 0812345678</p>
                </div>
                <div class="FBILInput2 input-group-lg">
                    <label for="name">Contact's email address</label>
                    <input type="text" class="form-control" id="name" name="name">
                    <p class="FBILIFoot">e.g. email@example.com</p>
                </div>
                <div class="FBILISubmit">
                	<button type="submit" class="btn btn-warning" onclick="window.location='?page=flight-booking/booking-review';">Continue</button>
                </div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="FBIRight">
			<div class="FBIRHead">
				<p>Departure</p>
				<p>Thursday, 1 September 2018</p>
			</div>
			<div class="FBIRBody row">
				<div class="col-xs-5">
					<img src="assets/images/garuda-icon.png">
				</div>
				<div class="col-xs-7">
					<p>Garuda Indonesia</p>
					<p>GA-1210</p>
					<p>Business</p>
				</div>
				<div class="col-xs-1 FBIRBPoint">
					<p>o</p>
					<div class="FBIRBPointLine"></div>
					<p>o</p>
				</div>
				<div class="col-xs-4 FBIRBPoint">
					<div>
						<p>18:00</p>
						<p>Thu, 01 Sep</p>
					</div>
					<div class="FBRIBPBottom">
						<p>20:18</p>
						<p>Thu, 01 Sep</p>						
					</div>
				</div>
				<div class="col-xs-7 FBIRBPoint">
					<div>
						<p>Jakarta (CGK)</p>
						<p>Soekarno Hatta Internation Airport</p>
					</div>
					<div class="FBRIBPBottom2">
						<p>Bali/Denpasar (DPS)</p>
						<p>Ngurah Rai Int'l</p>						
					</div>				
				</div>
			</div>
		</div>
		<div class="FBIRight">
			<div class="FBIRHead">
				<p>Departure</p>
				<p>Thursday, 1 September 2018</p>
			</div>
			<div class="FBIRBody row">
				<div class="col-xs-5">
					<img src="assets/images/garuda-icon.png">
				</div>
				<div class="col-xs-7">
					<p>Garuda Indonesia</p>
					<p>GA-1210</p>
					<p>Business</p>
				</div>
				<div class="col-xs-1 FBIRBPoint">
					<p>o</p>
					<div class="FBIRBPointLine"></div>
					<p>o</p>
				</div>
				<div class="col-xs-4 FBIRBPoint">
					<div>
						<p>18:00</p>
						<p>Thu, 04 Sep</p>
					</div>
					<div class="FBRIBPBottom">
						<p>20:18</p>
						<p>Thu, 04 Sep</p>						
					</div>
				</div>
				<div class="col-xs-7 FBIRBPoint">
					<div>
						<p>Jakarta (CGK)</p>
						<p>Soekarno Hatta Internation Airport</p>
					</div>
					<div class="FBRIBPBottom2">
						<p>Bali/Denpasar (DPS)</p>
						<p>Ngurah Rai Int'l</p>						
					</div>				
				</div>
			</div>
		</div>
		
	</div>
</div>
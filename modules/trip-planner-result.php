<?php include('modules/trip-planner-header.php'); ?>
<div class="TPRSection2">
	<div class="TPRS2Item row">
		<div class="col-sm-5">
			<img src="assets/images/bali.jpg" alt="">
		</div>
		<div class="col-sm-7 TRPS2Desc">
			<div class="col-xs-3">
				<p class="TPRS2ICaption">Itenarary</p>
				<p>1 Hotel</p>
				<p>8 Attraction</p>
				<p>2 Beach</p>
				<p>5 Restaurant</p>
			</div>
			<div class="col-xs-5">
				<p class="TPRS2ICaption">Connecting Flight</p>
				<p>Airport: <b>Soekarna Hatta intl. Airport</b></p>
				<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
				<p>Depart: <b>Jakarta, Indonesia</b></p>
				<p>Arrival: <b>Denpasar, Indonesia</b></p>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 2,200,000</b></span>
			</div>
			<div class="col-xs-4">
				<p class="TPRS2ICaption">Novotel hotel</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<div class="row">
					<div class="col-xs-4">
						<span class="glyphicon glyphicon-bed"></span>
					</div>
					<div class="col-xs-8">
						Superior Double or Twin Room with Balcony
					</div>
				</div>
				<p class="TPRS2IAfterBed">Include breakfast</p>
				<span class="pull-right TPRS2IPriceThrough">Rp. 1,800,000</span><br>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 1,500,000</b></span>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-5 TPRS2Col2">
			<span><b>Bali</b>, Indonesia</span>
			<div class="pull-right TPRS2Weather">
				<div><span class="glyphicon glyphicon-cloud"></span></div>
				<div>
					<center>
						<p>18&#8451;</p>
						<p>Day</p>
					</center>
				</div>
				<div>
					<center>
						<p>11&#8451;</p>
						<p>Night</p>
					</center>
				</div>
			</div>
		</div>
		<div class="col-sm-7 TPRS2Col2">
			<div class="pull-right">
				<button class="btn btn-default btn-lg" onclick="window.location='?page=trip-planner-detail';">View detail</button><span><b>Rp. 5,700,000</b></span>
			</div>
		</div>
	</div>

	<div class="TPRS2Item row">
		<div class="col-sm-5">
			<img src="assets/images/bali.jpg" alt="">
		</div>
		<div class="col-sm-7 TRPS2Desc">
			<div class="col-xs-3">
				<p class="TPRS2ICaption">Itenarary</p>
				<p>1 Hotel</p>
				<p>8 Attraction</p>
				<p>2 Beach</p>
				<p>5 Restaurant</p>
			</div>
			<div class="col-xs-5">
				<p class="TPRS2ICaption">Connecting Flight</p>
				<p>Airport: <b>Soekarna Hatta intl. Airport</b></p>
				<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
				<p>Depart: <b>Jakarta, Indonesia</b></p>
				<p>Arrival: <b>Denpasar, Indonesia</b></p>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 2,200,000</b></span>
			</div>
			<div class="col-xs-4">
				<p class="TPRS2ICaption">Novotel hotel</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<div class="row">
					<div class="col-xs-4">
						<span class="glyphicon glyphicon-bed"></span>
					</div>
					<div class="col-xs-8">
						Superior Double or Twin Room with Balcony
					</div>
				</div>
				<p class="TPRS2IAfterBed">Include breakfast</p>
				<span class="pull-right TPRS2IPriceThrough">Rp. 1,800,000</span><br>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 1,500,000</b></span>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-5 TPRS2Col2">
			<span><b>Bali</b>, Indonesia</span>
			<div class="pull-right TPRS2Weather">
				<div><span class="glyphicon glyphicon-cloud"></span></div>
				<div>
					<center>
						<p>18&#8451;</p>
						<p>Day</p>
					</center>
				</div>
				<div>
					<center>
						<p>11&#8451;</p>
						<p>Night</p>
					</center>
				</div>
			</div>
		</div>
		<div class="col-sm-7 TPRS2Col2">
			<div class="pull-right">
				<button class="btn btn-default btn-lg" onclick="window.location='?page=trip-planner-detail';">View detail</button><span><b>Rp. 5,700,000</b></span>
			</div>
		</div>
	</div>

	<div class="TPRS2Item row">
		<div class="col-sm-5">
			<img src="assets/images/bali.jpg" alt="">
		</div>
		<div class="col-sm-7 TRPS2Desc">
			<div class="col-xs-3">
				<p class="TPRS2ICaption">Itenarary</p>
				<p>1 Hotel</p>
				<p>8 Attraction</p>
				<p>2 Beach</p>
				<p>5 Restaurant</p>
			</div>
			<div class="col-xs-5">
				<p class="TPRS2ICaption">Connecting Flight</p>
				<p>Airport: <b>Soekarna Hatta intl. Airport</b></p>
				<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
				<p>Depart: <b>Jakarta, Indonesia</b></p>
				<p>Arrival: <b>Denpasar, Indonesia</b></p>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 2,200,000</b></span>
			</div>
			<div class="col-xs-4">
				<p class="TPRS2ICaption">Novotel hotel</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<div class="row">
					<div class="col-xs-4">
						<span class="glyphicon glyphicon-bed"></span>
					</div>
					<div class="col-xs-8">
						Superior Double or Twin Room with Balcony
					</div>
				</div>
				<p class="TPRS2IAfterBed">Include breakfast</p>
				<span class="pull-right TPRS2IPriceThrough">Rp. 1,800,000</span><br>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 1,500,000</b></span>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-5 TPRS2Col2">
			<span><b>Bali</b>, Indonesia</span>
			<div class="pull-right TPRS2Weather">
				<div><span class="glyphicon glyphicon-cloud"></span></div>
				<div>
					<center>
						<p>18&#8451;</p>
						<p>Day</p>
					</center>
				</div>
				<div>
					<center>
						<p>11&#8451;</p>
						<p>Night</p>
					</center>
				</div>
			</div>
		</div>
		<div class="col-sm-7 TPRS2Col2">
			<div class="pull-right">
				<button class="btn btn-default btn-lg" onclick="window.location='?page=trip-planner-detail';">View detail</button><span><b>Rp. 5,700,000</b></span>
			</div>
		</div>
	</div>

	<div class="TPRS2Item row">
		<div class="col-sm-5">
			<img src="assets/images/bali.jpg" alt="">
		</div>
		<div class="col-sm-7 TRPS2Desc">
			<div class="col-xs-3">
				<p class="TPRS2ICaption">Itenarary</p>
				<p>1 Hotel</p>
				<p>8 Attraction</p>
				<p>2 Beach</p>
				<p>5 Restaurant</p>
			</div>
			<div class="col-xs-5">
				<p class="TPRS2ICaption">Connecting Flight</p>
				<p>Airport: <b>Soekarna Hatta intl. Airport</b></p>
				<p><img src="assets/images/garuda-icon.png"> Garuda Indonesia</p>
				<p>Depart: <b>Jakarta, Indonesia</b></p>
				<p>Arrival: <b>Denpasar, Indonesia</b></p>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 2,200,000</b></span>
			</div>
			<div class="col-xs-4">
				<p class="TPRS2ICaption">Novotel hotel</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<div class="row">
					<div class="col-xs-4">
						<span class="glyphicon glyphicon-bed"></span>
					</div>
					<div class="col-xs-8">
						Superior Double or Twin Room with Balcony
					</div>
				</div>
				<p class="TPRS2IAfterBed">Include breakfast</p>
				<span class="pull-right TPRS2IPriceThrough">Rp. 1,800,000</span><br>
				<span>1 Ticket</span> <span class="pull-right"><b>Rp. 1,500,000</b></span>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-5 TPRS2Col2">
			<span><b>Bali</b>, Indonesia</span>
			<div class="pull-right TPRS2Weather">
				<div><span class="glyphicon glyphicon-cloud"></span></div>
				<div>
					<center>
						<p>18&#8451;</p>
						<p>Day</p>
					</center>
				</div>
				<div>
					<center>
						<p>11&#8451;</p>
						<p>Night</p>
					</center>
				</div>
			</div>
		</div>
		<div class="col-sm-7 TPRS2Col2">
			<div class="pull-right">
				<button class="btn btn-default btn-lg" onclick="window.location='?page=trip-planner-detail';">View detail</button><span><b>Rp. 5,700,000</b></span>
			</div>
		</div>
	</div>
	<center>
		<button class="btn btn-info btn-lg TPRS2IMore">GET MORE RESULTS</button>
	</center>
</div>
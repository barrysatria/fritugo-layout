<div class="row FBInformation">
	<div class="col-sm-12">
		<h3>Hotel Booking</h3>
		<div class="FBILeft">
			<a href="#" class="HotelBookingHeadSpan">Login</a> to your Fritugo account to easy access and other advantages.
		</div>
	</div>
	<div class="clearfix">
	</div>
	
	<div class="col-sm-8">
		<h4>Contact Information</h4>
		<div class="FBILeft">
			<div class="FBILInput1 input-group-lg">
                <label for="name">Contact Name</label>
                <input type="text" class="form-control" id="name" name="name">
                <p class="FBILIFoot">As on ID Card/passport (without degree or special characters)</p>
            </div>
            <div class="FBILInput2 form-group input-group-lg">
                <label for="name">Contact's mobile phone number</label>
                <input type="text" class="form-control" id="name" name="name">
                <p class="FBILIFoot">e.g. +62812345678, for Country Code (+62) and Mobile No. 0812345678</p>
            </div>
            <div class="FBILInput2 input-group-lg">
                <label for="name">Contact's email address</label>
                <input type="text" class="form-control" id="name" name="name">
                <p class="FBILIFoot">e.g. email@example.com</p>
            </div>
            <div class="checkbox HBCheckbox">
			  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>I am the guest</label>
			</div>
			<div class="FBILInput1 input-group-lg">
                <label for="name">Guest Fullname</label>
                <input type="text" class="form-control" id="name" name="name">
                <p class="FBILIFoot">Fill your name as on ID Card/passport/driving licence (without degree or special characters)</p>
            </div>
			<div>
				<div class="HBPAdd">Add special request</div>
				<div class="HBPSpecial">Special request (optional)</div>
				<div class="HBPExplain">Can't stand of the smell of cigarette smoke, or need an extra bed? fill in your request here and the properti will do it best to meet your wish.</div>
			</div>
		</div>
		<div class="FBILeft">
			<p class="HBPSpecial">Cancellation policy</p>
			<p class="HBPExplain">This reservation is not refundable.</p>
		</div>
		<h4>Price Details</h4>
		<div class="FBILeft">
			<p class="HBPSpecial">Mulia resort</p>
			<div class="row HBPrice">
				<div class="col-xs-8">
					Royal Suite Ocean Court - Domestic & Kitas Holder Only (2 Night) x1
				</div>
				<div class="col-xs-4">
					Rp. 8.754.050
				</div>
				<div class="col-xs-8">
					Taxt Recovery Charges
				</div>
				<div class="col-xs-4">
					Rp. 1.838.850
				</div>
				<div class="col-xs-8">
					Fritugo Fee
				</div>
				<div class="col-xs-4">
					<span>FREE</span>
				</div>
			</div>
			<hr>
			<div class="row HBTotalPrice">
				<div class="col-xs-8">
					Total
				</div>
				<div class="col-xs-4">
					Rp. 10.592.400
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				By clicking the button, you agree to Fritugo's<br>
				<a href="#" title="">Terms & Conditions</a> and <a href="#" title="">Privacy Policy</a>
			</div>
			<div class="col-md-4">
				<button onclick="window.location='?page=hotel-booking/booking-review';" type="submit" class="btn btn-warning pull-right">Continue To Payment</button>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="FBIRight">
			<div class="HBRHead">
				<p>Booking Details</p>
			</div>
			<div class="HBRBody row">
				<div class="col-xs-4">
					<img src="assets/images/afgan.jpg">
				</div>
				<div class="col-xs-8">
					<p>Novoltel Nusa Dua</p>
					<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				</div>
				<div class="clearfix"></div>
				<div class="HBRBodyBottom">
					<span><i class="fa fa-cutlery"></i> Free Breakfast</span><span><i class="fa fa-wifi"></i> Free WiFi</span>
				</div>
				<hr />
				<div>
					<span>Duration of Stay</span><span class="pull-right">2 Nights</span>
				</div>
				<div class="clearfix"></div>
				<div>
					<span>Check-in</span><span class="pull-right">Thu, 1 Sep 2018</span>
				</div>
				<div class="clearfix"></div>
				<div>
					<span>Check-out</span><span class="pull-right">Sun, 4 Sep 2018</span>
				</div>
				<div class="clearfix"></div>
				<div>
					<span>Room type</span><span class="pull-right">Royal Suite Ocean<br>Court - Domestic &<br>Kitas Holder Only</span>
				</div>
				<div class="clearfix"></div>
				<div>
					<span>No. of room</span><span class="pull-right">1 room</span>
				</div>
				<div class="clearfix"></div>
				<div>
					<span>Guest per room</span><span class="pull-right">2 guests</span>
				</div>
			</div>
		</div>
		
		
	</div>
</div>
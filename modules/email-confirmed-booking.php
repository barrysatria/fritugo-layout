<?php 
	$subject = 'Confirmed Booking';
	$body = '
	<!DOCTYPE html>
	<html>
	<head>
		<style type="text/css" media="screen">
			@import url("https://fonts.googleapis.com/css?family=Ubuntu");
			body {
				font-family: "Ubuntu";
			}
		</style>
	</head>
	<body style="margin: 0; padding: 0;">
		<table style="width: 100%; background-color: #4c6f8f;">
			<tr>
				<td style="padding: 0; margin: 0;"></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td style="width: 50%; padding: 30px;"><img src="http://fritugo.project-testserver.com/template/assets/images/logo.png"></td>
				<td style="width: 50%; padding: 30px; text-align: right;"><img src="http://fritugo.project-testserver.com/template/assets/images/email-logo.jpg"></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td colspan="2" style="text-align: center;">
					<p style="color: #6259a8; font-size: 30px; margin-bottom: 40px;">Your booking is confirmed and complete!</p>
					<p style="color: #696969; font-size: 20px;">Your Booking ID: 118573024</p>
					<p style="color: #6a6a6a; font-size: 15px;">You can easily manage your booking with our self service.</p>
				</td>
			</tr>
			<tr>
				<td style="width: 50%; text-align:right; padding: 10px;">
					<button style="color: white; background-color: #6259aa; width: 250px; padding: 10px; border: 0; font-size: 20px;">Manage my Booking</button>
				</td>
				<td style="width: 50%; text-align:left; padding: 10px;">
					<button style="color: white; background-color: #6259aa; width: 250px; padding: 10px; border: 0; font-size: 20px;">My itinerary</button>
				</td>
			</tr>
		</table>
		<hr />
		avra
	</body>
	</html>
	';
 ?>
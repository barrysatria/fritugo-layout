<?php include('modules/hotel-search.php');?>
<div class="resultSection row">
	<div class="col-sm-3 RSCol1">
		<p class="RSCol1Caption">Sort filters</p>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Highest Price</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Lowest Price</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Review Score</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Popularity</label>
		</div>
		<hr>
		<p class="RSCol1Caption">Star Rating</p>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><span class="fa fa-star"></span></label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><span class="fa fa-star"></span><span class="fa fa-star"></span></label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></label>
		</div>
		<hr>
		<p class="RSCol1Caption">Facilities</p>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Wi-Fi</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Swimming Pool</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Parking Lot</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Restaurant</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>24-Hour Front Desk</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Elevator</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Wheelchair Access</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Fitness Center</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Meeting Facilites</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Airport Transfer</label>
		</div>
		<hr>
		<p class="RSCol1Caption">Preferences</p>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Luxury</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Budget</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Py at Hotel</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Villa</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Apartement</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Award Winners</label>
		</div>
		<hr>
		<p class="RSCol1Caption">Type</p>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Hotel</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Resort</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Villa</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Apartement</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Homestay</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Hostel</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Guest House</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>B & B</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Meeting Facilities</label>
		</div>
		<div class="checkbox">
		  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Camping
		  	</label>
		</div>
		<hr>
		<p class="RSCol1Caption">Price Range Per Night</p>
		<div class="RSCol1PriceRange">
			<span id="RSCol1PriceRangeLeft" class="pull-left">Rp. 0</span><span>-</span><span id="RSCol1PriceRangeRight" class="pull-right">Rp. 20000000</span>
		</div>
		<div class="checkbox">
			<label><input id="FilterHotelPrice" type="text"/></label>
		</div>
	</div>
	<div class="col-sm-9 RSCol2">
		<div class="col-xs-9 RSCol2Tab">
			<div class="RSCol2Tab1">
				<p>Cheapest</p>
				<p class="RSColTabPrice">Rp. 782,000</p>
			</div>
			<div class="RSCol2Tab2">
				<p>Recommended</p>
				<p class="RSColTabPrice">Rp. 812,000</p>
			</div>
			<div class="RSCol2Tab3">
				<p>Popuplar</p>
				<p class="RSColTabPrice">Rp. 925,000</p>
			</div>
		</div>
		<!-- <div class="col-xs-3">
			<div class="RSCol2Caption">
				Choose<br>
				Departure Flight
			</div>
		</div> -->
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H1">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H1">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H2">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H2">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H3">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H3">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H4">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H4">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H5">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H5">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
		<div class="col-xs-12 RHCol2Result" data-toggle="collapse" data-target="#H6">
			<div class="RHCol2Foto">
				<img src="assets/images/novotel-room.jpg" alt="">
			</div>
			<div class="RHCol2Desc">
				<p>Novotel Nusa Dua</p>
				<p><span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
				<p>Nusa Dua<br>Bali</p>
				<div>
					<p>Fritugo Rating: <span>8.9</span> / 10</p>
				</div>
			</div>
			<div class="RHCol2Price">
				<p class="RSCol2PriceText">Rp. 782,000</p>
				<p class="RSCol2PriceDetail">1 Ticket: Rp. 782,000</p>
				<button onclick="window.location='?page=hotel-detail';" class="btn RSCol2PriceButton" type="submit">BUY</button>
			</div>

			<div class="RHCol2Caret">
				<span class="caret"></span>
			</div>
		</div>
		<div class="col-xs-12 RHCol2ResultToggle row collapse" id="H6">
			<div class="col-xs-8">
				<p>Night(s) 1 (September 1,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 2 (September 2,2018) <span class="pull-right">Rp. 987,500</span></p>
				<p>Night(s) 3 (September 3,2018) <span class="pull-right">Rp. 987,500</span></p>
				<hr>
				<p>Price excluding tax <span class="pull-right">Rp. 2,962,500</span></p>
				<p>Taxes and other fees <span class="pull-right">Rp. 622,125</span></p>
				<p class="RHCol2Fee">Fritugo fee <span class="pull-right">FREE</span></p>
			</div>
			<div class="col-xs-4">
				<p>Total Payment</p>
				<p>Rp. 3,854,625</p>
			</div>
		</div>
	</div>
</div>
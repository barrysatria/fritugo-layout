<div class="TPRSection">
	<div class="TPRHead row">
		<div class="col-xs-2">
			<div>
				<span>Depart</span>
				<span class="TPRHTime">01.09.2018</span>
			</div>
			<div>
				<span>Return</span>
				<span class="TPRHTime">04.09.2018</span>
			</div>
		</div>
		<div class="col-xs-6">
			<div>
				Flight from <b>Jakarta, Indonesia</b>
			</div>
			<div>
				<b>1 Person</b> with a budget of <b>Rp. 7,400,000</b>
			</div>
		</div>
		<div class="col-xs-4">
			<div class="pull-right">
				<b>Sort by</b>
				<select name="">
					<option value="">Recommended</option>
					<option value="">Rating</option>
					<option value="">Lowest price</option>
				</select>
			</div>
		</div>
	</div>
</div>
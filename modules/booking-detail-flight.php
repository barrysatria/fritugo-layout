<a href="?page=user/booking" class="UserBack">< &nbsp; &nbsp;Back to My Booking</a>
<div class="BookingDetail">
	<div class="BDCaption">
		Booking Details
	</div>
	<hr>
	<div class="row">
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking by
			</div>
			<div class="BDOutput">
				Jon Snow
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking Date
			</div>
			<div class="BDOutput">
				17 April 2018
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Booking ID
			</div>
			<div class="BDOutput">
				78346334
			</div>
		</div>
	</div>
	<hr>
	<div class="BDCaption">
		PAYMENT
	</div>
	<div class="row">
		<div class="col-xs-4">
			<div class="BDLabel">
				Payment Methods
			</div>
			<div class="BDOutput">
				Transfer
			</div>
		</div>
		<div class="col-xs-4">
			<div class="BDLabel">
				Payment Status
			</div>
			<div class="BDOutput">
				<button class="BDTicket">TICKET ISSUED</button>
			</div>
		</div>
	</div>
	<div>
		<hr class="BDHRDashed">
	</div>
	<div>
		<button class="BDReceipt">Receipt</button>
	</div>
</div>
<div class="BookingDetail">
	<div class="BDCaption">
		Flight Details
	</div>
	<hr>
	<div>
		<div class="pull-right">
			<span>PNR Code</span><span class="BDPNRCode">MFK4HJ</span><br>
			<button class="BDReceipt">Download Ticket</button>
		</div>
		<img src="assets/images/airasia-sm.png" class="BDImg" alt=""> AirAsia XT-7515
		<div class="clearfix"></div>
		<div>
			Booking Status<br><br>
			<button class="BDTicket">E-TICKET ISSUED</button>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-8">
				<div class="col-xs-5">
					<div>Ngurah Rai Int'l</div><br>
					<div class="BDLabel">Bali / Denpasar (DPS)</div>
				</div>
				<div class="col-xs-2"><i class="fa fa-long-arrow-right BDArrow" aria-hidden="true"></i></div>
				<div class="col-xs-5">
					<div>Soekarno Hatta Int'l</div><br>
					<div class="BDLabel">Jakarta (CGK)</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="BDLabel">Flight Schedule</div>
				<div>Sunday, 18 September 2018<br>
				18:25 - 19:05</div>
			</div>
		</div>
		<hr>
		<p class="BDPassenger">List of Passenger(s)</p>
		<p class="BDLabel">1. Mr. Jon Snow</p>
	</div>
	
	
</div>
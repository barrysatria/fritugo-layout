<nav class="navbar resultSearch">
    <div class="container-fluid">
    	<form class="navbar-form navbar-left">
    		<div class="form-group">
    			<div class="input-group input-group col-xs-6">
                    <input type="text" class="form-control" placeholder="Departure" />
                    <span class="input-group-btn" style="width:0px;"></span>
                    <input type="text" class="form-control" placeholder="Destination" />
                </div>
                <div class="input-group input-group col-xs-3">
                	<input type="text" class="form-control" id="depart" name="depart" placeholder="depart">
                	<span class="input-group-btn" style="width:0px;"></span>
                	<input type="text" class="form-control" id="return" name="return" placeholder="return">
                </div>
                <div class="input-group input-group col-xs-2">
                    <select class="form-control" name="person" style="border-radius: 5px;">
                        <option selected disabled>Person</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
    		</div>
    		<button type="submit" class="btn btn-warning">FIND FLIGHT</button>
    	</form>
    </div>
</nav>
<div class="FBookingSection">
	<div class="FBSBreadcrumb">
		<p>HELLO THERE! WHAT CAN WE HELP YOU?</p>
	</div>
</div>
<div class="ContactSection">
	<div class="CS1">
		Fritugo's ever growing popularity is generating a lot of buzz!<br>
		If there something you'd like to clear up, fell free to drop us a line.
	</div>
	<div class="row CS2">
		<div class="col-sm-5">
			<p>Reach us</p>
			<p><i class="fa fa-map-marker" aria-hidden="true"></i> Address</p>
			<p>Indonesia</p>
			<p>Fritu Corp. ONE PM Building Level 5, Boulevard Gading Serpong, Tangerang 15810, Banten, Indonesia</p>

			<p><i class="fa fa-at" aria-hidden="true"></i> Email</p>
			<p>hi@fritugo.com</p>
		</div>
		<div class="col-sm-7">
			<p>Leave a message</p>
			<div class="col-xs-6">
				<div class="input-group-lg">
                    <label for="name">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name">
                </div>
			</div>
			<div class="col-xs-6">
				<div class="input-group-lg">
                    <label for="name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name">
                </div>
			</div>
			<div class="col-xs-6">
				<div class="input-group-lg">
                    <label for="name">Email Address</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
			</div>
			<div class="col-xs-6">
				<div class="input-group-lg">
                    <label for="name">Subject</label>
                    <input type="email" class="form-control" id="subject" name="subject">
                </div>
			</div>
			<div class="col-xs-12">
				<div class="input-group-lg">
                    <label for="name">Message</label>
                    <input type="email" class="form-control" id="message" name="message">
                </div>
			</div>
			<button class="btn btn-warning btn-lg">Submit</button>
			<p class="CS2Sosmed">Stay Connected</p>
			<p>
				<img src="assets/images/facebook.png" alt="">
				<img src="assets/images/twitter.png" alt="">
				<img src="assets/images/linkedin.png" alt="">
			</p>
		</div>
	</div>
	<div class="CS3">
		<iframe class="CS3Map" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD7PszmE-siLglBQpbu2t5IvpmaVBGGkXQ&q=one+pm+lifestyle+building+serpong" frameborder="0" allowfullscreen></iframe>
	</div>
</div>
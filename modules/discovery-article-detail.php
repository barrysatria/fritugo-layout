<div class="Discovery">
	<div class="DBreadcrumb">
		<div>Discovery / Article / Introducing Indonesia</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DiscoveryArticle">
		<p class="DACaption">Introducing Indonesia</p>
		<hr>
		<div class="DASocmedFb"><i class="fa fa-facebook" aria-hidden="true"></i></div>
		<div class="DASocmedTwitter"><i class="fa fa-twitter" aria-hidden="true"></i></div>
		<div class="DASocmedGPlus"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
		<div class="DASocmedMail"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
		<div class="DASocmedPrint"><i class="fa fa-print" aria-hidden="true"></i></div>
		<div class="DASocmedCaption">SEND THIS</div>
		<hr>
		<div>
			<p class="DAP1">Indonesia's numbers astound: more than 17,000 islands, of which 8000 are inhabited, and over 300 languages are spoken across them. It's a beguiling country offering myriad adventures.</p>

			<p class="DAP2">BEACHES & VOLCANOES</p>

			<p class="DAP3">Venturing across Indonesia you’ll see a dramatic landscape, as diverse as those living upon it. Sulawesi's wildly multilimbed coastline embraces white-sand beaches and diving haunts, while Sumatra is contoured by a legion of nearly 100 volcanoes marching off into the distance, several capable of erupting at any time.</p>

			<p class="DAP2">RICH DIVERSITY</p>

			<p class="DAP3">The world’s fourth most populous country – 255 million and counting – is a sultry kaleidoscope that runs along the equator for 5000km. From the western tip of Sumatra to the eastern edge of Papua, this nation defies homogenisation. It is a land of so many cultures, peoples, animals, customs, plants, sights, artworks and foods that it is like 100 countries melded into one.
			The people are as radically different from each other as if they came from different continents, with every island a unique blend of the men, women and children who live upon it. Over time deep and rich cultures have evolved, from the mysteries of the spiritual Balinese to the utterly non-Western belief system of the Asmat people of Papua.</p>

			<p class="DAP2">AMAZING SPECTACLE</p>

			<p class="DAP3">Dramatic sights are the norm. There’s the sublime: an orangutan lounging in a tree. The artful: a Balinese dancer executing precise moves that would make a robot seem loose-limbed. The idyllic: a deserted stretch of blinding white sand on Sumbawa set off by azure surf breaks. The astonishing: the mobs in a cool, glitzy Jakarta mall on a Sunday. The intriguing: the too-amazing-for-fiction tales of the twisted history of the beautiful Banda Islands. The heart-stopping: the ominous menace of a Komodo dragon. The humbling: a woman bent double with a load of firewood on Sumatra. The delicious: a south Bali restaurant. The shocking: the funeral ceremonies of Tana Toraja. The solemn: the serene magnificence of Borobudur.</p>
			
			<p class="DAP2">GREAT ADVENTURE</p>

			<p class="DAP3">This ever-intriguing, ever-intoxicating land offers some of the last great adventures on earth. Sitting in the open door of a train whizzing across Java, idling away time on a ferry bound for Kalimantan, hanging on to the back of a scooter on Flores, rounding the mystifying corner of an ancient West Timor village or simply trekking through wilderness you’re sure no one has seen before – you’ll enjoy endless exploration of the infinite diversity of Indonesia’s 17,000-odd islands.</p>
		</div>
		<hr>
		<div class="DASocmedFb"><i class="fa fa-facebook" aria-hidden="true"></i></div>
		<div class="DASocmedTwitter"><i class="fa fa-twitter" aria-hidden="true"></i></div>
		<div class="DASocmedGPlus"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
		<div class="DASocmedMail"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
		<div class="DASocmedPrint"><i class="fa fa-print" aria-hidden="true"></i></div>
		<div class="DASocmedCaption">SEND THIS</div>
		<hr>
	</div>
</div>

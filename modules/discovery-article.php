<div class="Discovery">
	<div class="DBreadcrumb DBHome">
		<div><span class="glyphicon glyphicon-globe"></span> Explore The World</div>
		<div class="pull-right">
			<a href="?page=discovery/all">All destinations</a>
			<a href="#" class="active">Tips & articles</a>
		</div>
	</div>
</div>
<div class="DiscoveryContent">
	<div class="DiscoveryArticle">
		<div class="row">
			<div class="col-xs-9">
				<p class="DACaption">Travel tips & articles</p>
				<div class="DAFilterButton">
					<button class="DAFilterButton"><span>Food and Drink</span> (1171)</button>
					<button class="DAFilterButton"><span>Adventure travel</span> (987)</button>
					<button class="DAFilterButton"><span>Heritage and history</span> (940)</button>
					<button class="DAFilterButton"><span>Art and culture</span> (854)</button>
					<button class="DAFilterButton"><span>Wildlife and nature</span> (747)</button>
				</div>
				<div class="row">
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-cambodia.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-bag.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-boat.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-jakarta.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-cambodia.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-bag.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-boat.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-jakarta.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-cambodia.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-bag.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-boat.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>
					<div class="col-sm-3 DAItem">
						<a href="?page=discovery/article-detail"><img src="assets/images/article-jakarta.jpg" alt=""></a>
						<p class="DAIP1">Article</p>
						<p class="DAIP2">Where to find delicious local food in Colombia</p>
						<p class="DAIP3">If there's one thing you can rely on when travelling  around Colombia, it's that you'll never get hungry ...</p>
					</div>

				</div>
				<div class="DAMore">
					<button>Show more</button>
				</div>

			</div>
			<div class="col-xs-3 DARight">
				<img src="assets/images/right-photo.jpg" alt="">
				<p class="DARCaption">Collections</p>
				<p class="DARItem">Destination top travel list for 2015</p>
				<hr>
				<p class="DARItem">Destination top travel list for 2014</p>
				<hr>
				<p class="DARItem">Destination top travel list for 2013</p>

			</div>
		</div>
	</div>
</div>
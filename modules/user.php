
<div class="User">
	<div class="UBreadcrumb">
		<div>PROFILE</div>
	</div>
</div>
<div class="UserContent">
	<div class="row">
		<div class="col-xs-3">
			<div class="UCNav <?php echo ($pieces[1] == 'profile' ? 'active' : ''); ?>" onclick="window.location='?page=user/profile';">
				<span class="fa fa-user-o" aria-hidden="true"></span> Edit Profile
				<div></div>
			</div>
			<div class="UCNav">
				<span class="fa fa-list-alt" aria-hidden="true"></span> My Itineraries
				<div></div>
			</div>
			<div class="UCNav <?php echo ($pieces[1] == 'card' ? 'active' : ''); ?>"  onclick="window.location='?page=user/card';">
				<span class="fa fa-credit-card" aria-hidden="true"></span> My Cards
				<div></div>
			</div>
			<div class="UCNav <?php echo ($pieces[1] == 'booking' || $pieces[1] == 'booking-detail-flight' || $pieces[1] == 'booking-detail-hotel' ? 'active' : ''); ?>"  onclick="window.location='?page=user/booking';">
				<span class="fa fa-list" aria-hidden="true"></span> My Booking
				<div></div>
			</div>
			<div class="UCNav <?php echo ($pieces[1] == 'traveler' ? 'active' : ''); ?>"  onclick="window.location='?page=user/traveler';">
				<span class="fa fa-users" aria-hidden="true"></span> Travelers Information
				<div></div>
			</div>
			<div class="UCNav <?php echo ($pieces[1] == 'newsletter' ? 'active' : ''); ?>"  onclick="window.location='?page=user/newsletter';">
				<span class="fa fa-newspaper-o" aria-hidden="true"></span> Newsletter Subscription
				<div></div>
			</div>
		</div>
		<div class="col-xs-9">
			
				<?php 
					if(!isset($pieces[1])){
						include('modules/user-profile.php');
					}
					elseif ($pieces[1] == 'profile') {
						include('modules/user-profile.php');
					}
					elseif ($pieces[1] == 'newsletter') {
						include('modules/user-newsletter.php');
					}
					elseif ($pieces[1] == 'card') {
						include('modules/user-card.php');
					}
					elseif ($pieces[1] == 'traveler') {
						include('modules/user-traveler.php');
					}
					elseif ($pieces[1] == 'booking') {
						include('modules/user-booking.php');
					}
					elseif ($pieces[1] == 'booking-detail-flight') {
						include('modules/booking-detail-flight.php');
					}
					elseif ($pieces[1] == 'booking-detail-hotel') {
						include('modules/booking-detail-hotel.php');
					}
				?>
			
		</div>
	</div>
</div>
<?php include('modules/hotel-search.php');?>
<div class="HotelDetail">
	<div class="HDSection1">
		<h2><b>NOVOTEL NUSA DUA</b></h2>
		<span class="fa fa-star"></span><span class="fa fa-star"></span></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>
		<p>Jalan Raya NUsa Dua Selatan Kawasan Sawangan Nusa Dua, Badung,<br>Indonesia, 80363</p>
	</div>
	<div class="row HDSection2">
		<div class="col-md-6 HDS2Left">
			<p><b>8.9</b> / 10</p>
			<p>Great</p>
			<p>According to <b>2,400</b> guests</p>
			<p><b>See review</b></p>
			<img src="assets/images/tripadvisor.jpg" alt="">
		</div>
		<div class="col-md-6 HDS2Right">
			<p>Starting From</p>
			<p>Rp. 1,250,000</p>
			<button>Book Now</button>
		</div>
		<div class="clearfix">
			
		</div>
		<div class="col-sm-2">
			<i class="fa fa-snowflake-o" aria-hidden="true"></i> AC
		</div>
		<div class="col-sm-2">
			<i class="fa fa-car" aria-hidden="true"></i> Parking
		</div>
		<div class="col-sm-2">
			<i class="fa fa-clock-o" aria-hidden="true"></i> 24-hour front desk
		</div>
		<div class="col-sm-2">
			<i class="fa fa-cloud" aria-hidden="true"></i> Swimming pool
		</div>
		<div class="col-sm-2">
			<i class="fa fa-terminal" aria-hidden="true"></i> Elevator
		</div>
		<div class="col-sm-2">
			<i class="fa-spoon" aria-hidden="true"></i> Retaurant
		</div>
		<div class="col-sm-2">
			<i class="fa fa-wifi" aria-hidden="true"></i> Wi-Fi
		</div>
		
		<div class="clearfix">
			
		</div>
		<div col-sm-12>
			<hr>
		</div>
		<div class="col-sm-6">
			<div id="map"></div>
			<script>

			// The following example creates complex markers to indicate beaches near
			// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
			// to the base of the flagpole.

			function initMap() {
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 13,
					center: {lat: -8.80, lng: 115.2}
				});

				setMarkers(map);
			}

			// Data for the markers consisting of a name, a LatLng and a zIndex for the
			// order in which these markers should display on top of each other.
			var beaches = [
			['Mengiat Beach', -8.8093644, 115.231539, 1],
			['Pasar Senggol', -8.8074134, 115.2265792, 2],
			['Bali Colletion Shopping Center', -8.8036038, 115.2266885, 3],
			['Puja Mandala', -8.800614, 115.2119153, 4]
			];

			function setMarkers(map) {
			// Adds markers to the map.

			// Marker sizes are expressed as a Size of X,Y where the origin of the image
			// (0,0) is located in the top left of the image.

			// Origins, anchor positions and coordinates of the marker increase in the X
			// direction to the right and in the Y direction down.
			var image = {
				url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(20, 32),
			// The origin for this image is (0, 0).
			origin: new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at (0, 32).
			anchor: new google.maps.Point(0, 32)
			};
			// Shapes define the clickable region of the icon. The type defines an HTML
			// <area> element 'poly' which traces out a polygon as a series of X,Y points.
			// The final coordinate closes the poly by connecting to the first coordinate.
			var shape = {
				coords: [1, 1, 1, 20, 18, 20, 18, 1],
				type: 'poly'
			};
			for (var i = 0; i < beaches.length; i++) {
				var beach = beaches[i];
				var marker = new google.maps.Marker({
					position: {lat: beach[1], lng: beach[2]},
					map: map,
					icon: image,
					shape: shape,
					title: beach[0],
					zIndex: beach[3]
				});
			}
			}
			</script>
			
		</div>
		<div id="rightMaps" class="col-sm-6">
			<p>Nearby Attractions</p>
			<div class="col-xs-2">
				1
			</div>
			<div class="col-xs-10">
				<p><b>Mengiat Beach</b></p>
				<p>(1.30 km)</p>
			</div>
			<div class="clearfix">
			
			</div>
			<div class="col-xs-2">
				2
			</div>
			<div class="col-xs-10">
				<p><b>Pasar Senggol</b></p>
				<p>(1.30 km)</p>
			</div>
			<div class="clearfix">
			
			</div>
			<div class="col-xs-2">
				3
			</div>
			<div class="col-xs-10">
				<p><b>Bali Collection Shopping Center</b></p>
				<p>(1.30 km)</p>
			</div>
			<div class="clearfix">
			
			</div>
			<div class="col-xs-2">
				4
			</div>
			<div class="col-xs-10">
				<p><b>Puja Mandala</b></p>
				<p>(1.30 km)</p>
			</div>
		</div>
	</div>
	<div class="HDSection3">
		<div class="row">
			<div class="col-xs-2">
				<img src="assets/images/novotel-room.jpg">
			</div>
			<div class="col-xs-5">
				<p>Royal Suit Ocean Court</p>
				<p>Max. guests 2 person(s)</p>
				<p>
					<span><i class="fa fa-list"></i> Cancelation policies applies</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
				</p>
			</div>
			<div class="col-xs-3">
					<p><span>Rp. 6,300,000</span> / room / night(s)</p>
					<p>Rp. 6,100,000</p>
					<p>Price inclusive of tax</p>
			</div>
			<div class="col-xs-2">
				<button>Book Now!</button>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-2">
				<img src="assets/images/novotel-room.jpg">
			</div>
			<div class="col-xs-5">
				<p>Royal Suit Ocean Court</p>
				<p>Max. guests 2 person(s)</p>
				<p>
					<span><i class="fa fa-list"></i> Cancelation policies applies</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
				</p>
			</div>
			<div class="col-xs-3">
					<p><span>Rp. 6,300,000</span> / room / night(s)</p>
					<p>Rp. 6,100,000</p>
					<p>Price inclusive of tax</p>
			</div>
			<div class="col-xs-2">
				<button>Book Now!</button>
			</div>
		</div>
		<hr>

		<div class="row">
			<div class="col-xs-2">
				<img src="assets/images/novotel-room.jpg">
			</div>
			<div class="col-xs-5">
				<p>Royal Suit Ocean Court</p>
				<p>Max. guests 2 person(s)</p>
				<p>
					<span><i class="fa fa-list"></i> Cancelation policies applies</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
					<span><i class="fa fa-cutlery"></i> Free breakfest</span>
				</p>
			</div>
			<div class="col-xs-3">
					<p><span>Rp. 6,300,000</span> / room / night(s)</p>
					<p>Rp. 6,100,000</p>
					<p>Price inclusive of tax</p>
			</div>
			<div class="col-xs-2">
				<button>Book Now!</button>
			</div>
		</div>
	</div>
	<div class="HDSection4">
		<ul class="nav nav-tabs">
			<li class="HDS4TabHead active"><a data-toggle="tab" href="#reviewFritugo"><span>Review by Fritugo users</span></a></li>
			<li class="HDS4TabHead"><a data-toggle="tab" href="#reviewTripadvisor"><span>Review Tripadvisor</span></a></li>
		</ul>
		<div class="tab-content">
			<div  id="reviewFritugo" class="tab-pane fade in active">
				<div class="row">
					<div class="col-xs-3">
						<div class="HDS4Circle"><span class="HDS4CircleP1">8,9</span><br><span class="HDS4CircleP2">/10</span></div>
					</div>
					<div class="col-xs-9">
						<table class="HDS4Table">
							<tr>
								<td class="HDS4TableTd1">
									Cleanliness	
								</td>
								<td class="HDS4TableTd2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100" style="width: 94%;">
											<span class="sr-only">60% Complete</span>
										</div>
									</div>
								</td>
								<td class="HDS4TableTd3">
									9.40
								</td>
							</tr>
							<tr>
								<td class="HDS4TableTd1">
									Comfort	
								</td>
								<td class="HDS4TableTd2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
											<span class="sr-only">92% Complete</span>
										</div>
									</div>
								</td>
								<td class="HDS4TableTd3">
									9.24
								</td>
							</tr>
							<tr>
								<td class="HDS4TableTd1">
									Meal	
								</td>
								<td class="HDS4TableTd2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="91" aria-valuemin="0" aria-valuemax="100" style="width: 91%;">
											<span class="sr-only">91% Complete</span>
										</div>
									</div>
								</td>
								<td class="HDS4TableTd3">
									9.11
								</td>
							</tr>
							<tr>
								<td class="HDS4TableTd1">
									Location	
								</td>
								<td class="HDS4TableTd2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;">
											<span class="sr-only">87% Complete</span>
										</div>
									</div>
								</td>
								<td class="HDS4TableTd3">
									8.79
								</td>
							</tr>
							<tr>
								<td class="HDS4TableTd1">
									Service	
								</td>
								<td class="HDS4TableTd2">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width: 88%;">
											<span class="sr-only">88% Complete</span>
										</div>
									</div>
								</td>
								<td class="HDS4TableTd3">
									8.87
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="HDS4ReviewHead">
					<span><b>280</b> Fritugo customers have stayed in this hotel and given reviews:</span>
					<div class="btn-group pull-right">
						<select class="form-control">
						    <option value="one">See all review</option>
						    <option value="two">See most rate review</option>
						</select>
					</div>
				</div>
				<div class="HDS4ReviewList">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">8.5</div>
								</td>
								<td>
									<p><b>Andre J.</b><span class="HDS4RIDate"> - Aug 28, 2017</span></p>
									<p>Great five star hotel with calm and private ambience</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">9.4</div>
								</td>
								<td>
									<p><b>Hilda S.</b><span class="HDS4RIDate"> - Aug 21, 2017</span></p>
									<p>Good, statisfied with all the service</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">8.5</div>
								</td>
								<td>
									<p><b>Andre J.</b><span class="HDS4RIDate"> - Aug 28, 2017</span></p>
									<p>Great five star hotel with calm and private ambience</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">9.4</div>
								</td>
								<td>
									<p><b>Hilda S.</b><span class="HDS4RIDate"> - Aug 21, 2017</span></p>
									<p>Good, statisfied with all the service</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">8.5</div>
								</td>
								<td>
									<p><b>Andre J.</b><span class="HDS4RIDate"> - Aug 28, 2017</span></p>
									<p>Great five star hotel with calm and private ambience</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">9.4</div>
								</td>
								<td>
									<p><b>Hilda S.</b><span class="HDS4RIDate"> - Aug 21, 2017</span></p>
									<p>Good, statisfied with all the service</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">8.5</div>
								</td>
								<td>
									<p><b>Andre J.</b><span class="HDS4RIDate"> - Aug 28, 2017</span></p>
									<p>Great five star hotel with calm and private ambience</p>
								</td>
							</tr>
						</table>
					</div>
					<hr class="HDS4RHR">
					<div class="HDS4RItem">
						<table>
							<tr>
								<td>
									<div class="HDS4RICircle">9.4</div>
								</td>
								<td>
									<p><b>Hilda S.</b><span class="HDS4RIDate"> - Aug 21, 2017</span></p>
									<p>Good, statisfied with all the service</p>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="HDS4Pagination pull-right">
					<span>First</span><span>Prev</span><span><div class="HDS4PState">1</div></span><span>2</span><span>3</span><span>...</span><span>28</span><span>Next</span><span>Last</span>
				</div>
				<div class="clearfix">
				</div>
				<div class="HDS4Tab">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#hotelFacility">Hotel facility</a></li>
						<li><a data-toggle="tab" href="#description">Description</a></li>
						<li><a data-toggle="tab" href="#hotelPolicy">Hotel policy</a></li>
					</ul>
					<div class="clearfix">
					</div>
					<div class="tab-content">
						<div  id="hotelFacility" class="tab-pane fade in active">
							<div class="row">
								<div class="col-sm-4">
									<p><i class="fa fa-cutlery"></i> <b>Food and Drinks</b></p>
									<ul>
										<li>A la carte lunch</li>
										<li>Restaurant with AC</li>
										<li>Bar</li>
										<li>Breakfast</li>
										<li>BUffet breakfast</li>
										<li>Brunch</li>
										<li>Buffet dinner</li>
										<li>Cafe</li>
										<li>Night club</li>
										<li>Poolside bar</li>
										<li>Set menu dinner</li>
										<li>Set menu lunch</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-list"></i> <b>Things to Do</b></p>
									<ul>
										<li>Children play area</li>
										<li>Children pool</li>
										<li>Entertainment for children</li>
										<li>Fitness center</li>
										<li>Health club</li>
										<li>Massage</li>
										<li>Outdoor pool</li>
										<li>Pool cabanas</li>
										<li>Private beach</li>
										<li>SPA</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-bed"></i> <b>In-room Facilities</b></p>
									<ul>
										<li>Bathrobe</li>
										<li>Bathtub</li>
										<li>Desk</li>
										<li>DVD player</li>
										<li>Haridryer</li>
										<li>Minibar</li>
										<li>Shower</li>
										<li>TV</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<p><i class="fa fa-suitcase"></i> <b>Hotel Services</b></p>
									<ul>
										<li>Porter</li>
										<li>Concierge</li>
										<li>Doorman</li>
										<li>24-hour receptionist</li>
										<li>24-hour security</li>
										<li>laundry service</li>
										<li>Multilingual staff</li>
										<li>Weeding service</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-building"></i> <b>Public Facilities</b></p>
									<ul>
										<li>Parking</li>
										<li>Elevator</li>
										<li>24-hour room service</li>
										<li>Restaurant</li>
										<li>Room service</li>
										<li>Safety deposit box</li>
										<li>Wifi in public area</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-building"></i> <b>General</b></p>
									<ul>
										<li>AC</li>
										<li>Ballroom</li>
										<li>Banquet</li>
										<li>Family room</li>
										<li>Non-smoking room</li>
										<li>Terrace</li>
										<li>Wifi in public area</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<p><i class="fa fa-laptop"></i> <b>Business Facilities</b></p>
									<ul>
										<li>Business center</li>
										<li>Conference room</li>
										<li>Meeting facility</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-wifi"></i> <b>Connectivity</b></p>
									<ul>
										<li>Internet point</li>
										<li>LAN internet with surcharge</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-futbol-o"></i> <b>Sport and Recreations</b></p>
									<ul>
										<li>Tennis</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<p><i class="fa fa-wheelchair"></i> <b>Accessibilty</b></p>
									<ul>
										<li>Accessible bathroom</li>
									</ul>
								</div>
								<div class="col-sm-4">
									<p><i class="fa fa-bus"></i> <b>Shuttle Service</b></p>
									<ul>
										<li>Airport transfer with surcharge</li>
									</ul>
								</div>
							</div>
							<center>
								<button class="HDS4BookButton" onclick="window.location='?page=hotel-booking/booking-review';">Book Now!</button>
							</center>
							<div class="HDS4Information">
								<table>
									<tr>
										<td><div class="HDS4InformationIcon">i</div></td>
										<td><b>Disclaimer:</b> it is the hotel's responsibility to ensure that all photo are accurate. Fritugo will not be held responsible for any photo are inaccureate.</td>
									</tr>
								</table>
							</div>
						</div>
						<div id="description" class="tab-pane fade">
							asdfghjklqwertyuiopzxcvbnm
						</div>
						<div id="hotelPolicy" class="tab-pane fade">
							asdfghjklqwertyuiopzxcvbnm
						</div>
					</div>
				</div>
			</div>
			<div id="reviewTripadvisor" class="tab-pane fade">
				aasd
			</div>
		</div>
	</div>
</div>
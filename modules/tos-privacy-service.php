<p class="TOSCaption">Privacy Policy and Cookies</p>
<p>FRITUGO attaches great importance to maintaining privacy of your data.  We appreciate the confidence given to us and undertake an obligation to protect any personal information provided to us.  In this document, we describe how we use and process personal data and use cookies.</p>

<p>This Privacy Policy describes the principles of how we will process personally identifiable information and personal information (hereinafter referred to as "the Personal Data"), which we collect in respect of you, or which you provide to us.  This Policy shall apply to our websites, applications, tools, promotions, electronic mail messages, accounts for logging in to the system and any other Services we provide (hereinafter referred to as "FRITUGO Services").</p>

<p>As a visitor or user of FRITUGO Services, you are entitled to know and understand our methods of handling confidential information before you provide us any personal information.</p>

<p>By using FRITUGO Services, you do hereby agree to use of your Personal Data by us in compliance with this Policy.</p>
<p class="TOSSubCaption">What Information Do We Collect?</p>
<p>We collect information provided by you, such as name, electronic mail addresses, and password, when you register in order to obtain or use of any of our Services.  This, for instance, happens when you register an account, participate in FRITUGO promotions, or subscribe to notices of prices, mailing lists and other messages.  If you make a booking via FRITUGO functionality, you may be suggested to specify supplementary information, such as address, passport number, and credit card data.</p>

<p>We automatically collect certain information passively because of your use of FRITUGO Services.  This information is bound to device, which you use to access the FRITUGO Services.  Such information includes, among other things, your IP-address, operating system, web browser type, URL, from which you went, web page views, access time and your location.</p>

<p>We also collect information provide to us when you start using FRITUGO Services by going from a third-party application, a social network (e.g., Facebook or Twitter) or from another website (e.g., partner promotions).</p>
<p class="TOSSubCaption">Disclosure of Information</p>
<p>We will never sell your Personal Data to a third party; however, in some cases we may provide them to third parties in compliance with this Policy or if we must do it by law.</p>

<p>We may provide your information to partner companies within a FRITUGO Group for use in compliance with this Policy and the foregoing purposes.  We also may transmit your information to some third parties for its processing on behalf of us.  By way of example, we may provide your electronic mail address, along with other data collected by us, to trusted independent companies, which will help us identify the trends and adapt FRITUGO Services and messages, which we send to you.</p>

<p>We demand that such third parties (some of which may be located outside of the European Economic Area) strictly comply with our instructions and reserve the use of your Personal Data for own commercial purposes of such companies, unless you give your explicit consent to such their use.  Any third parties, which are provided access to your data in the course of provision of the Services on behalf of us, shall be subject to strict contractual limitations in order to provide protection of your data and their processing exclusively in compliance with the applicable data security and confidentiality laws.  We may also conduct independent audit of such service providers in order to assure that they are compliant with our standards.</p>

<p>If we offer promotions jointly with third parties, then we may from time to time agree to provision of electronic mail addresses of the participants to the partner, which conducts the contest.  If this is the case, this will be clearly specified in the terms of the promotion and you will be suggested to agree that your data will be so used at the time when you perform the acts required for the participation in the promotion.  We shall bear no responsibility whatsoever for further use or dissemination of electronic mail addresses by our partners.</p>

<p>When we redirect you to websites of third-party service provider, which arrange travels, to make a booking, we may use your Personal Data to autocomplete some information on such website of third-party companies, in order to make the process of booking more simple and efficient.</p>

<p>In addition, we may disclose your Personal Data, when this is required in order to ensure compliance with our term of provision of the Services or other agreements and transmit them a prospective and ultimate buyer in case of sale to FRITUGO Company (in whole or in part).</p>

<p>We may also provide anonymous aggregate information about use to other parties.</p>
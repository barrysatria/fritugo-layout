<nav class="navbar <?php echo (isset($home) ? 'home-nav' : ''); ?>">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><img src="assets/images/logo<?php echo (isset($home) ? '-white' : ''); ?>.png" alt="" class="logo <?php echo (isset($home) ? '' : 'logo-nohome'); ?>"></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="<?php echo ($path == 'trip-planner' || $path == 'trip-planner-result' || $path == 'trip-planner-detail' ? 'active' : ''); ?>"><a href="?page=trip-planner">TRIP PLANNER</a></li>
            <li class="<?php echo ($path == 'home-flight' || $path == 'flight-result' || $path == 'flight-booking' ? 'active' : ''); ?>"><a href="?page=home-flight">FLIGHTS</a></li>
            <li class="<?php echo ($path == 'hotel-home' || $path == 'hotel-result' || $path == 'hotel-detail' ? 'active' : ''); ?>"><a href="?page=hotel-home">HOTELS</a></li>
            <li class="<?php echo ($path == 'discovery' ? 'active' : ''); ?>"><a href="?page=discovery/all">DISCOVERY</a></li>
            <li class="<?php echo ($path == 'itineraries' ? 'active' : ''); ?>"><a href="?page=itineraries">ITINERARIES</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="<?php echo ($path == '' ? 'home-nav' : ''); ?>"><a href="#"><span class="glyphicon glyphicon-search"></span></a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">LOGIN / REGISTER</span></a>
                <div class="dropdown-menu header-login" id="loginDD">
                    <p>Log In</p>
                    <hr>
                    <center><img src="assets/images/facebook-login.png" alt="" style="width: 150px"></center>
                    <hr class="secondHr">
                    <center><div class="HLOr">OR </div></center>
                    <div>
                        <label for="name">Email</label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <br>
                    <div>
                        <label for="name">Password</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <div class="checkbox inline-block">
                        <label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Remember Me</label>
                    </div>
                    <div class="inline-block nav-forgot">
                        <a href="#" title="">Forgot Password?</a>
                    </div>
                    <button class="btn btn-warning btn-block">Log In</button>
                    <hr>
                    <center>
                        Don't have acconut? <a onclick="showSignup();">Sign Up</a>
                    </center>

                </div>
                <div class="dropdown-menu header-signup" id="SignupDD">
                    <p>Sign Up <a href="#" class="pull-right" onclick="closeSignup();"><i class="fa fa-times" aria-hidden="true"></i></a></p>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-control" name="firstname" placeholder="enter first name">
                        </div>
                        <div class="col-xs-6">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="form-control" name="lastname" placeholder="enter last name">
                        </div>
                        <div class="col-xs-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="enter email">
                        </div>
                        <div class="col-xs-6">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="enter password">
                        </div>
                        <div class="col-xs-6">
                            <button class="btn btn-warning btn-block">Log In</button>
                        </div>
                        <div class="col-xs-6">
                            <a onclick="closeSignup();">Login if already registered</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">IDR <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">USD</a></li>
                    <li><a href="#">EUR</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">EN <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">ID</a></li>
                </ul>
            </li>
            
        </ul>
    </div>
</nav>
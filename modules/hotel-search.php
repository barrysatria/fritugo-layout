<nav class="navbar resultSearch">
    <div class="container-fluid">
    	<form class="navbar-form navbar-left">
    		<div class="form-group">
    			<div class="input-group input-group col-xs-6">
                    <input type="text" class="form-control" placeholder="Check-In" />
                    <span class="input-group-btn" style="width:0px;"></span>
                    <input type="text" class="form-control" placeholder="Check-Out" />
                </div>
                <div class="input-group input-group col-xs-3">
                	<input type="text" class="form-control" id="depart" name="Person" placeholder="Person">
                	<span class="input-group-btn" style="width:0px;"></span>
                	<input type="text" class="form-control" id="return" name="Room" placeholder="Room">
                </div>
                <div class="input-group input-group col-xs-2">
                    <input type="text" class="form-control" placeholder="City, Hotel" />
                </div>
    		</div>
    		<button type="submit" class="btn btn-warning">FIND HOTEL</button>
    	</form>
    </div>
</nav>
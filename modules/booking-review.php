<div class="row FBReview">
	<div class="col-sm-12">
		<h2>Please Review Your Booking</h2>
		<div class="FBRHead">
			Please check your booking detail then click <b>Continue to Payment</b>
		</div>
	</div>
	<div class="col-sm-8">
		<h3>Flight Details</h3>
		<div class="FBRLeft">
			<div class="FBRLHead">
				<p>Departure</p>
				<p>Thursday, 1 September 2018</p>
				<div class="row">
					<div class="col-sm-7">
						<div class="col-xs-4 FBIRBPoint">
							<div>
								<p>18:00</p>
								<p>Thu, 01 Sep</p>
							</div>
							<div class="FBRBPBottom">
								<p>20:18</p>
								<p>Thu, 01 Sep</p>						
							</div>
						</div>
						<div class="col-xs-1 FBIRBPoint">
							<p>o</p>
							<div class="FBIRBPointLine"></div>
							<p>o</p>
						</div>
						<div class="col-xs-7 FBIRBPoint">
							<div>
								<p>Jakarta (CGK)</p>
								<p>Soekarno Hatta Internation Airport</p>
							</div>
							<div class="FBRBPBottom2">
								<p>Bali/Denpasar (DPS)</p>
								<p>Ngurah Rai Int'l</p>						
							</div>				
						</div>
					</div>
					<div class="col-sm-5">
						<img src="assets/images/garuda-icon.png">
						<p class="FBRLHeadP1">Garuda Indonesia - GA-1010</p>
						<p class="FBRLHeadP2">Business</p>
					</div>
				</div>
			</div>
		</div>
		<div class="FBRLeft">
			<div class="FBRLHead">
				<p>Return</p>
				<p>Thursday, 4 September 2018</p>
				<div class="row">
					<div class="col-sm-7">
						<div class="col-xs-4 FBIRBPoint">
							<div>
								<p>18:00</p>
								<p>Thu, 04 Sep</p>
							</div>
							<div class="FBRBPBottom">
								<p>20:18</p>
								<p>Thu, 04 Sep</p>						
							</div>
						</div>
						<div class="col-xs-1 FBIRBPoint">
							<p>o</p>
							<div class="FBIRBPointLine"></div>
							<p>o</p>
						</div>
						<div class="col-xs-7 FBIRBPoint">
							<div>
								<p>Bali/Denpasar (DPS)</p>
								<p>Ngurah Rai Int'l</p>
							</div>
							<div class="FBRBPBottom2">
								<p>Jakarta (CGK)</p>
								<p>Soekarno Hatta Internation Airport</p>					
							</div>				
						</div>
					</div>
					<div class="col-sm-5">
						<img src="assets/images/garuda-icon.png">
						<p class="FBRLHeadP1">Garuda Indonesia - GA-1010</p>
						<p class="FBRLHeadP2">Business</p>
					</div>
				</div>
			</div>
		</div>
		<h3>List of passenger(s)</h3>
		<div class="FBRLeft row">
			<h4>1. Mr. Jhon Snow</h4>
			<div class="col-xs-4">
				Date of birth
			</div>
			<div class="col-xs-7">
				22 november 1986
			</div>
			<div class="col-xs-4">
				Baggage CGK - DPS
			</div>
			<div class="col-xs-7">
				20kg
			</div>
			<div class="col-xs-4">
				Baggage DPS - CGK
			</div>
			<div class="col-xs-7">
				15kg
			</div>
		</div>
		<h3>Price Details</h3>
		<p class="FBRParagraph">Price are subject to change to adjust to ariline reservation system.<br>
			The confirmed price is shown on <b>review page</b></p>
		<div class="FBRLeft FBRTextRight row">
			<div class="col-xs-8">
				Garuda Indonesia (adult) x1 :
			</div>
			<div class="col-xs-4">
				Rp 1.598.300
			</div>
			<div class="col-xs-8">
				Baggage CKG - DPS :
			</div>
			<div class="col-xs-4">
				Rp 0
			</div>
			<div class="col-xs-8">
				Garuda Indonesia (adult) x1 :
			</div>
			<div class="col-xs-4">
				Rp 944.100
			</div>
			<div class="col-xs-8">
				Baggage DPS - CGK :
			</div>
			<div class="col-xs-4">
				Rp 0
			</div>
			<div class="col-xs-8">
				Unique Code :
			</div>
			<div class="col-xs-4">
				Rp -465
			</div>
		</div>
		<div class="FBRLeftTotal FBRTextRight row">
			<div class="col-xs-8">
				Garuda Indonesia (adult) x1 :
			</div>
			<div class="col-xs-4">
				Rp 1.598.300
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				By clicking the button below, you agree to Fritugo's<br>
				<a href="#" title="">Terms & Conditions</a> and <a href="#" title="">Privacy Policy</a>
			</div>
			<div class="col-md-4">
				<button type="submit" class="btn btn-warning pull-right">Continue To Payment</button>
			</div>
		</div>
	</div>
	<div class="col-sm-4">

		<div class="FBRRight row">
			<div class="col-xs-6">
				Booking ID
			</div>
			<div class="col-xs-6 text-right">
				<b>2004140237</b>
			</div>
		</div>
		<div class="FBRRightBottom">
			<h3>Continue to payment</h3>
			<p>By clicking the button below, you agree to Fritugo's <a href="#" title="">Terms & Conditions</a> and <a href="#" title="">Privacy Policy</a></p>
			<button type="submit" class="btn btn-warning">Continue To Payment</button>
		</div>
		
	</div>
</div>

<div class="UCInner">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#profileLogin">Login ID</a></li>
		<li><a data-toggle="tab" href="#profileChange">Change Password</a></li>
	</ul>
	<div class="clearfix">
	</div>
	<div class="tab-content">
		<div  id="profileLogin" class="tab-pane fade in active">
			<div class="UCProfileInput">
				<div class="pull-right">Change</div>
				<p class="UCProfileInput1">Full Name</p>
				<p class="UCProfileInput2">Bryan Putra Wangsa</p>
			</div>
			<div class="UCProfileInput">
				<p>Enable login verifivation code</p>
				<div class="checkbox">
				  	<label><input type="checkbox" value=""><span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Send me verfication code every time i login from new device</label>
				</div>
			</div>
			<div class="UCProfileInput">
				<div class="pull-right">+ Phone</div>
				<p class="UCProfileInput3">Phone</p>
				<div class="UCProfileInputGrey">
					<div class="pull-right">Remove</div>
					<span class="UCPIGTlp">+6281280680632</span>
					<span class="UCPIGNotif">Recipient for notification</span>
				</div>
				<p class="UCProfileInput1">Mobile number to receive account-related notification.</p>
				<hr>
				<div class="pull-right">+ Email Adress</div>
				<p class="UCProfileInput3">Email Adress</p>
				<div class="UCProfileInputGrey">
					<div class="pull-right">Remove</div>
					<span class="UCPIGTlp">bryanwangsaa@gmail.com</span>
					<span class="UCPIGNotif">Recipient for notification</span>
				</div>
				<p class="UCProfileInput1">Email address to receive account-related notification.</p>
				<hr>
				<p class="UCProfileInput3">Linked Account</p><br>
				<p class="UCProfileInput1">For an easy login, connect your social account with Fritugo.</p>
				<div class="UCProfileInputWhite">
					<div class="pull-right">Unlink</div>
					<span>Facebook</span>
					<span class="UCPIGNotif">connected</span>
				</div>
				<div class="UCProfileInputWhite">
					<div class="pull-right">Unlink</div>
					<span>Google</span>
					<span class="UCPIGNotif">connected</span>
				</div>
			</div>
		</div>
		<div  id="profileChange" class="tab-pane fade">
			<p class="UCPICaption">Please enter password you have not used in any other sites</p>
			<div class="UCProfileInput">
				<p class="UCProfileInput1">Old password</p>
				<input type="password" class="UCPIPassword" value="********">
			</div>
			<div class="UCProfileInput">
				<p class="UCProfileInput1">New Password</p>
				<input type="password" class="UCPIPassword">
			</div>
			<div class="UCProfileInput">
				<p class="UCProfileInput1">Confirm New Password</p>
				<input type="password" class="UCPIPassword">
			</div>
			<button class="UCPIButton">Change Password</button>

		</div>
	</div>
</div>
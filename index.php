<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        <meta http-equiv="imagetoolbar" content="no" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1,mozilla=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Fritugo</title>
        <link rel="stylesheet" type="text/css" href="assets/css/loader.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugin/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap-slider/css/bootstrap-slider.min.css">
        <link rel="stylesheet" type="text/css" href="assets/plugin/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="assets/plugin/jcarousel/css/jcarousel.responsive.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css" />
        <script crossorigin src="https://unpkg.com/react@16.1.0/umd/react.development.js"></script>
        <script crossorigin src="https://unpkg.com/react-dom@16.1.0/umd/react-dom.development.js"></script>
        <script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js"></script>
    </head>
    <body>
        
        <?php
            if (!isset($_GET['page'])) {
                $path = 'trip-planner';
                $home = 1;
            }
            if (isset($_GET['page'])) {
                $pieces = explode("/", $_GET['page']);
                if ($pieces[0] == 'trip-planner') {
                    $path = 'trip-planner';
                    $home = 1;
                }
                if ($pieces[0] == 'trip-planner-result') {
                    $path = 'trip-planner-result';
                }
                if ($pieces[0] == 'trip-planner-detail') {
                    $path = 'trip-planner-detail';
                }
                if ($pieces[0] == 'home-flight') {
                    $path = 'home-flight';
                    $home = 1;
                }
                if ($pieces[0] == 'flight-result') {
                    $path = 'flight-result';
                }
                if ($pieces[0] == 'flight-booking') {
                    $path = 'flight-booking';
                }
                if ($pieces[0] == 'hotel-home') {
                    $path = 'hotel-home';
                    $home = 1;
                }
                if ($pieces[0] == 'hotel-result') {
                    $path = 'hotel-result';
                }
                if ($pieces[0] == 'hotel-detail') {
                    $path = 'hotel-detail';
                }
                if ($pieces[0] == 'hotel-booking') {
                    $path = 'hotel-booking';
                }
                if ($pieces[0] == 'tos-privacy') {
                    $path = 'tos-privacy';
                }
                if ($pieces[0] == 'contact-us') {
                    $path = 'contact-us';
                }
                if ($pieces[0] == 'discovery') {
                    $path = 'discovery';
                }
                if ($pieces[0] == 'user') {
                    $path = 'user';
                }
                if ($pieces[0] == 'itineraries') {
                    $path = 'itineraries';
                }
                
            }
            include('modules/header.php');
            include('modules/'.$path.'.php');
            include('modules/footer.php');
        ?>
    
        
        <script type="text/javascript" src="assets/plugin/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="assets/plugin/moment/moment.min.js"></script>
        <script type="text/javascript" src="assets/plugin/numeral/numeral.min.js"></script>
        <script type="text/javascript" src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/plugin/lazysizes/lazysizes.min.js"></script>
        <script type="text/javascript" src="assets/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
        <script type="text/javascript" src="assets/plugin/jcarousel/js/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="assets/plugin/jcarousel/js/jcarousel.responsive.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzYZa8MeUJ2l2MBCjHtMG37sjjHGI7sTY&callback=initMap&hl=en"> </script>
        <script type="text/javascript" src="assets/js/main.js"></script>

    </body>
</html>
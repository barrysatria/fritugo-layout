// go to side 
function showSignup(){
	$("#loginDD").css("display", "none");
	$("#SignupDD").css("display", "block");
}
function closeSignup(){
	$("#loginDD").css("display", "block");
	$("#SignupDD").css("display", "none");
}
$( "#goToSlide" ).click(function() {
	$("#TPSdata1").css("display", "block");
	$("#TPSdata2").css("display", "none");
});

// Change carousal control
var carouselEl = $('.carousel');
var carouselItems = carouselEl.find('.item');
carouselEl.carousel({
  interval: false
}).on('slid.bs.carousel', function (event) {
	var itemStat = carouselItems.siblings('.active').index();
	if(itemStat == 1 || itemStat == "1"){
		$("#tripPlannerSlide > a.left").css("display", "block");
		$("#tripPlannerSlide > a.right > .glyphicon-chevron-right").toggleClass('glyphicon-chevron-right glyphicon-search');
	}
	if(itemStat == 0 || itemStat == "0"){
		$("#tripPlannerSlide > a.left").css("display", "none");
		$("#tripPlannerSlide > a.right > .glyphicon-search").toggleClass('glyphicon-search glyphicon-chevron-right');
	}
	if(itemStat == 2 || itemStat == "2"){
		$("#tripPlannerSlide > a.left").css("display", "none");
		$("#tripPlannerSlide > a.right").css("display", "none");
		window.location.href = "?page=trip-planner-result";
	}
});

//for login form not close in header if click something
$(".header-login").click(function(e) {
    e.stopPropagation();
});

//for ammount control in slide trip planner
// $("#tripPlanner").slider({ id: "sliderTripPlanner", step: 500000, min: 0, max: 20000000, value: 1000000 });
// $("#tripPlanner").on("slide", function(slideEvt) {
// 	$("#TPAmount").val(slideEvt.value);
// });

//for change time range in result flight
// $("#departDeparture").slider({min: 0, max: 50, range: true, value: [3, 40] });
// $("#departArrival").slider({min: 0, max: 50, range: true, value: [22, 30] });
// $("#returnDeparture").slider({min: 0, max: 50, range: true, value: [3, 40] });
// $("#returnArrival").slider({min: 0, max: 50, range: true, value: [22, 30] });

// //For change price range filter in result hotel
// $("#FilterHotelPrice").slider({min: 0, max: 20000000, range: true, value: [0, 20000000] });
// $("#FilterHotelPrice").on("slide", function(slideEvt) {
// 	$("#RSCol1PriceRangeLeft").text("Rp. "+slideEvt.value[0]);
// 	$("#RSCol1PriceRangeRight").text("Rp. "+slideEvt.value[1]);
// });

$(document).on('click', '.RSCol2PriceButton', function(e){
    e.stopPropagation(); 
});

//declare datepicker function
$(".datepicker").datepicker();


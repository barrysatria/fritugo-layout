var dataDay = [{
	date: '09-01-2018 00:00:00',
	destination: [{
		id: 1,
		name: 'Soekarno Hatta Int\'l. Airport',
		categoryId: 1,
		attractionStart: '06:00',
		attractionFinish: '06:40',
		ride: false,
		rideTime: false,
		uniquePlace: false
	},
	{
		id: 2,
		name: 'Ngurah Rai Airport',
		categoryId: 1, //flight etc..
		attractionStart: '06:40',
		attractionFinish: '06:40',
		ride: false,
		rideTime: false,
		uniquePlace: false
	},
	{
		id: 100,
		name: 'Novotel Bali',
		categoryId: 2, // hotel
		attractionStart: '08:20',
		attractionFinish: '15:00',
		ride: 'Taxi',
		rideTime: '20 mins',
		uniquePlace: false

	},
	{
		id: 80,
		name: 'Tanah Lot',
		categoryId: 7, // tempat wisata
		attractionStart: '15:40',
		attractionFinish: '19:00',
		ride: 'Taxi',
		rideTime: '36 mins',
		uniquePlace: true

	},
	{
		id: 100,
		name: 'Novotel Bali',
		categoryId: 2,
		attractionStart: 'last',
		attractionFinish: 'last',
		ride: 'Taxi',
		rideTime: '36 mins',
		uniquePlace: false

	}]
},
{
	date: '09-02-2018 00:00:00',
	destination: [{
		id: 100,
		name: 'Novotel Bali',
		categoryId: 2,
		attractionStart: 'first',
		attractionFinish: 'first',
		ride: false,
		uniquePlace: false

	},
	{
		id: 82,
		name: 'Kuta Beach',
		categoryId: 7,
		attractionStart: '07:00',
		attractionFinish: '17:00',
		ride: 'Taxi',
		rideTime: '50 mins',
		uniquePlace: true

	},
	{
		id: 70,
		name: 'Finn\'s Bar Club',
		categoryId: 9, //club
		attractionStart: '18:00',
		attractionFinish: '23:00',
		ride: 'Taxi',
		rideTime: '15 mins',
		uniquePlace: true

	},
	{
		id: 100,
		name: 'Novotel Bali',
		categoryId: 2,
		attractionStart: 'last',
		attractionFinish: 'last',
		ride: 'Taxi',
		rideTime: '1 hours 5 mins',
		uniquePlace: false

	}]
}];
var dataPopup = [
	{
		id: 1,
		name: 'Block Time',
		categoryId: 1,
		img: 'block-time.jpg',
		timeStart: false,
		timeFinish: false,
		price: false,
		uniquePlace: true,
		description: 'Use this option to block some personal time. You cannot add any activity during this priod.'
	},
	{
		id: 2,
		name: 'Tanah Lot Temple',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '07:00',
		timeFinish: '10:00',
		price: 24.25,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 3,
		name: 'Tirta Empul',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '10:00',
		timeFinish: '14:00',
		price: 45.00,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 4,
		name: 'Mount Agung',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '07:00',
		timeFinish: '10:00',
		price: 16.57,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 5,
		name: 'Mount Batur',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '04:00',
		timeFinish: '07:00',
		price: 80.21,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 6,
		name: 'Bali Bird Park',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '09:00',
		timeFinish: '13:00',
		price: 10.66,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 7,
		name: 'Bali Safari and Marine Park',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '08:00',
		timeFinish: '15:00',
		price: 32.88,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 8,
		name: 'Jimbaran Bay',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '14:00',
		timeFinish: '17:00',
		price: 65.08,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 9,
		name: 'Bali Zoo',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '09:00',
		timeFinish: '16:00',
		price: 50.00,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
	{
		id: 10,
		name: 'Jemeluk Beach',
		categoryId: 7,
		img: 'mount-agung.jpg',
		timeStart: '05:00',
		timeFinish: '10:00',
		price: 33.00,
		uniquePlace: true,
		description: 'This natural rock formation is also a home to pilgrimage temple pura tanah lot which. This natural rock formation is also a home to pilgrimage temple pura tanah lot which'
	},
];

class TripPlannerDetailSlide extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			dataDay: dataDay,
			popState: false,
			popTabCat: 1,
			popItem: [],
			addAttractionDayArrayId: null
		};
		this.addDay = this.addDay.bind(this);
		this.addAttraction = this.addAttraction.bind(this);
		this.closePopup = this.closePopup.bind(this);
		this.jqueryConfig = this.jqueryConfig.bind(this);
		this.deleteActivity = this.deleteActivity.bind(this);
	}

	addDay(){
		var lastDay = this.state.dataDay.length-1;
		var newData = {
			date: moment(this.state.dataDay[lastDay].date).add(1, 'd').format('MM-DD-YYYY HH:mm:ss'),
			destination: [this.state.dataDay[lastDay].destination[this.state.dataDay[lastDay].destination.length-1]]
		};
		// console.log(newData);

		this.setState({
		  	dataDay: this.state.dataDay.concat(newData),
		  	popState: true,
		  	popItem: dataPopup,
		  	addAttractionDayArrayId: this.state.dataDay.length
		});
	}

	addAttraction(event, data){
		
		var origin = this.state.dataDay[this.state.addAttractionDayArrayId].destination[this.state.dataDay[this.state.addAttractionDayArrayId].destination.length-1].name;
		var destination = data.name;
		var service = new google.maps.DistanceMatrixService();
		var rideTIme = '';
		service.getDistanceMatrix(
		{
		    origins: [origin],
		    destinations: [destination],
		    travelMode: 'DRIVING'
		}, result => {
		  	var rideTIme = result.rows[0].elements[0].duration.text;
			var dataAttractionNew = {
				id: data.id,
				name: data.name,
				categoryId: data.categoryId,
				attractionStart: data.timeStart,
				attractionFinish: data.timeFinish,
				ride: 'Taxi',
				rideTime: rideTIme,
				uniquePlace: data.uniquePlace
			};
			
			// console.log(this.state.dataDay[this.state.addAttractionDayArrayId]);
			var dataDayTemp = this.state.dataDay;
			var dataDestinationTemp = this.state.dataDay[this.state.addAttractionDayArrayId].destination;
			dataDestinationTemp = dataDestinationTemp.concat(dataAttractionNew);
			dataDayTemp[this.state.addAttractionDayArrayId].destination = dataDestinationTemp;
			this.setState({
				dataDay: dataDayTemp,
			  	popState: false
			});
		});
		// this.state.dataDay[this.state.addAttractionDayArrayId].destination.concat()
	}

	addActivity(index){
		this.setState({
		  	popState: true,
		  	addAttractionDayArrayId: index,
		  	popItem: dataPopup
		});
	}

	deleteActivity(dayId, activityId){
		var dataDayTemp = this.state.dataDay;
		dataDayTemp[dayId].destination.splice(activityId, 1);
		if(dataDayTemp[dayId].destination.length === 0){
			dataDayTemp.splice(dayId, 1);
		}
		this.setState({
			dataDay: dataDayTemp
		});
	}

	closePopup(e){
		this.setState({
		  	popState: false
		});
	}

	componentDidMount() {
	    this.jqueryConfig();
	}

	componentDidUpdate(){
		this.jqueryConfig();

	}

	// shouldComponentUpdate(){
	// 	this.jqueryConfig();
	// }

	jqueryConfig(){
		var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 3;
                } else if (width >= 350) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });

        $( ".TPD2IEvent" ).hover(
		  function() {
		    $( this ).find( ".TPD2IDelete" ).css( "display", "initial" );
		  }, function() {
		    $( this ).find( ".TPD2IDelete" ).css( "display", "none" );
		  }
		);
	}

	render() {

		var tripPlannerDetailPopup = "";
		if(this.state.popState == true){
			var tripPlannerDetailPopup = <TripPlannerDetailPopup dataDay={this.state.dataDay} closePopup={this.closePopup} addAttraction={this.addAttraction} data={this.state.popItem} tabActive={this.state.popTabCat} />
		}
		console.log(this.state.dataDay);
    	return (
	      	<div className="TPD2">
    			{tripPlannerDetailPopup}
				<h3>Itinerary</h3>
				<div className="jcarousel-wrapper">
				    <div ref="jcarousel" className="jcarousel">
				        <ul>
				            {
					            this.state.dataDay.map((day, index) => {
					                return (
					                    <li>
							            	<div className="TPD2Item">
												<p>Day {index + 1}</p><p className="pull-right">{moment(day.date).format('ddd, DD MMMM YYYY')}</p>
												{
													day.destination.map((destinationData, destinationIndex) => {

						                				return (
						                					<div>
						                						
															<TripPlannerDetailEvent data={destinationData} dayIndex={index} activityIndex={destinationIndex} deleteActivity={this.deleteActivity} />
																
															</div>
														)
													})
												}
												<div  onClick={(e) => this.addActivity(index)} className="TPD2IAdd pull-right">
													+
												</div>
							            	</div>
							            </li>
					                )
					            })
					        }
				        </ul>
				    </div>

				    <a href="#" className="jcarousel-control-prev">&lsaquo;</a>
				    <a href="#" className="jcarousel-control-next">&rsaquo;</a>

				</div>
				<button className="TPD2AddMore pull-right" onClick={this.addDay}>ADD MORE DAY</button>
				<div className="clearfix">
					
				</div>
				
			</div>
	    );
	}
}

class TripPlannerDetailPopup extends React.Component{
	
	render(){
		var activityList = [];
		this.props.dataDay.map((day, id) => {
			day.destination.map(activity => {
				var activityListDump = {
					id: activity.id,
					day: id
				}
				activityList.push(activityListDump);
			})
		})
		console.log(activityList);
		return(
      		<div className="TPDPop">
			  	<div className="TPDPContent">
			  		<div className="TPDPCHead">
			  			<span className="TPDPopCaption">Add activity to your day plan</span><span className="pop-close" onClick={this.props.closePopup}>&times;</span>
			  		</div>
			  		<hr />
			  		<span className={this.props.tabActive == 1 ? 'TPDPTab active' : 'TPDPTab'}><i className="fa fa-map-marker" aria-hidden="true"></i>Place</span>
			  		<span className={this.props.tabActive == 2 ? 'TPDPTab active' : 'TPDPTab'}><i className="fa fa-cutlery" aria-hidden="true"></i>Restaurant</span>
			  		<span className={this.props.tabActive == 3 ? 'TPDPTab active' : 'TPDPTab'}><i className="fa fa-building" aria-hidden="true"></i>Hotel</span>
			  		<span className={this.props.tabActive == 4 ? 'TPDPTab active' : 'TPDPTab'}><i className="fa fa-search" aria-hidden="true"></i>Custom Place</span>
			  		<div className="form-group has-feedback TPDSearch">
					    <input type="text" className="form-control input-sm" placeholder="Enter a City or Country" />
					    <i className="glyphicon glyphicon-search form-control-feedback ICSIcon"></i>
					</div>
					<div className="clearfix"></div>
					<div className="TPDPMenu">
						<div>
							<select className="TPDPMSelect">
								<option>Bali</option>
							</select>
						</div>
						<div>
							<select className="TPDPMSelect">
								<option>Select filter</option>
								<option>Popular</option>
								<option>Most rate</option>
								<option>Relevant</option>
							</select>
						</div>
					</div>
					<div className="row TPDPCItemList">
					{
						this.props.data.map(item => {
							return (
								<div className="col-sm-3">
								{item.timeStart ? (
									<div className="TPDPCItem" onClick={(e) => this.props.addAttraction(e, item)}>
										<div className="TPDPCItemTop">
											{
												activityList.map(list => {
													if(item.id === list.id){
														return (
															<div className="TPDPCBadge">Added to day {list.day+1} <div className="TPDPCBadgeCheck"><i className="fa fa-check" aria-hidden="true"></i></div></div>
														)
													}
												})
											}
											<img src={'assets/images/'+item.img} />
											
										</div>
										<div className="TPDPCItemBottom">
											<p className="TPDPCCaption">{item.name}</p>
											<p className="TPDPCTime">{item.timeStart} to {item.timeFinish}</p>
											<p>{ ((item.description).length > 100) ? 
											    (((item.description).substring(0,100-3)) + '...') : 
											    item.description }</p>
											<a className="TPDPCDetailButton">Details</a>
										</div>
										<hr />
										<div className="TPDPCPrice">
											<span>Tours from</span>
											<span className="pull-right"><b>USD</b> {item.price}</span>
										</div>
									</div>
							    ) : (
									<div className="TPDPCItem">
										<div className="TPDPCItemTop">
											<img src={'assets/images/'+item.img} />
										</div>
										<div className="TPDPCItemBottom">
											<p className="TPDPCCaption">{item.name}</p>
											<p>{item.description}</p>
										</div>
									</div>
							    )}
							    </div>
							)
						})
					}
					</div>
			  	</div>
			</div>
		);
	}
}

class TripPlannerDetailEvent extends React.Component{
	render(){
		var iconClass = '';
		if(this.props.data.categoryId == 1){
			iconClass = 'fa fa-plane';
		}else if(this.props.data.categoryId == 2){
			iconClass = 'fa fa-building';
		}
		else if(this.props.data.categoryId == 7){
			iconClass = 'fa fa-tree';
		}
		else if(this.props.data.categoryId == 9){
			iconClass = 'fa fa-beer';
		}
		return(
			<div>
				{this.props.activityIndex > 0 &&
			        <div className="TPD2ILine">
						{this.props.data.rideTime} <i className="fa fa-taxi"></i>
					</div>
			    }
				<div className="TPD2IEvent">
					<i className={iconClass}></i> {this.props.data.name} <span className="TPD2IDelete" onClick={(e) => this.props.deleteActivity(this.props.dayIndex, this.props.activityIndex)}>&times;</span>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
  <TripPlannerDetailSlide />,
  document.getElementById('TPD2')
);
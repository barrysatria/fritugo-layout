class InputRound extends React.Component {
	render() {
		return (
			<div className="input-group input-group-lg">
			<input type="text" className="form-control" placeholder="Departure" />
			<span className="input-group-btn" style={spanStyle}></span>
			<input type="text" className="form-control" placeholder="Destination" />
			</div>
		);
	}
}

class InputOneway extends React.Component {
	render() {
		return (
			<div className="form-group input-group-lg">
			<input type="text" className="form-control" placeholder="Departure" />
			</div>
		);
	}
}

class FlightHome extends React.Component {
	constructor(props) {
		super(props);
		this.state = {value: 'round'};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({value: event.target.value});
	}

	handleSubmit(event) {
		window.location='?page=flight-result';
	}

  	render() {
  		let inputFlight = null;
	  	if (this.state.value === "round") {
	     	inputFlight = <InputRound />;
	    } else {
	      	inputFlight = <InputOneway />;
	    }
	    return (
	      	<div className="HS1F">
		        <label className="radio-inline">
		            <input type="radio" name="flightRadio" checked={this.state.value === "round"} onChange={this.handleChange} value="round" />ROUNDTRIP
		        </label>
		        <label className="radio-inline">
		            <input type="radio" name="flightRadio" checked={this.state.value === "oneway"} onChange={this.handleChange} value="oneway" />ONE-WAY
		        </label>
		        <div className="HS1FI row">
		            <div className="col-sm-7">
		                <label>From</label>
		                {inputFlight}
		            </div>
		            <div className="col-sm-3">
		                <div className="input-group input-group-lg">
		                    <label for="depart">Depart</label>
		                    <input type="text" className="form-control" id="depart" name="depart" placeholder="depart" />
		                    <span className="input-group-btn" style={spanStyle}></span>
		                    <label for="return">Return</label>
		                    <input type="text" className="form-control" id="return" name="return" placeholder="return" />
		                </div>
		            </div>
		            <div className="col-sm-2">
		                <label>&nbsp;</label>
		                <div className="form-group input-group-lg">
		                    <select className="form-control" name="person">
		                        <option selected disabled>Person</option>
		                        <option>1</option>
		                        <option>2</option>
		                        <option>3</option>
		                        <option>4</option>
		                    </select>
		                </div>
		            </div>
		        </div>
		        <center><button type="button" className="btn btn-warning btn-lg" onClick={this.handleSubmit}>
		            FIND FLIGHT 
		        </button></center>
	        </div>
	    );
  }
}


var spanStyle = {
  	width:0
};

ReactDOM.render(
  <FlightHome />,
  document.getElementById('HS1F')
);
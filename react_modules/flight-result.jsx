var flightList = [
	{
		id: 1,
		takeoffTime: '09-01-2018 18:00:00',
		takeoffAirport: 'CKG',
		takeoffCity: 'Jakarta',
		takeoffProvince: 'Jakarta',
		landingTime: '09-01-2018 20:18:00',
		landingAirport: 'DPS',
		landingCity: 'Denpasar',
		landingProvince: 'Bali',
		flightTime: '2h 18m',
		flight: 'GA-408',
		price: 782000,
		person: 1
	},
	{
		id: 2,
		takeoffTime: '09-01-2018 17:00:00',
		takeoffAirport: 'CKG',
		takeoffCity: 'Jakarta',
		takeoffProvince: 'Jakarta',
		landingTime: '09-01-2018 19:20:00',
		landingAirport: 'DPS',
		landingCity: 'Denpasar',
		landingProvince: 'Bali',
		flightTime: '2h 20m',
		flight: 'GA-555',
		price: 777000,
		person: 1
	}
];

class FlightResult extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			tripOption: 'round',
			firstBuy: false,
			firstBuyData:{},
			secondBuy: false,
			secondBuyData:{}
		};
		this.handleBuy = this.handleBuy.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
	}
	handleBuy(event, data){
		if (this.state.firstBuy === false) {
			this.setState({
		      	firstBuy: true,
		      	firstBuyData: data
		    });
		}
		else{
			this.setState({
				secondBuy: true,
		      	secondBuyData: data
		    });
		}
		
	}
	handleCancel(){
		this.setState({
	      	firstBuy: false
	    });
	}
	handleCancelSecond(){
		this.setState({
	      	secondBuy: false
	    });
	}
	handleSubmit() {
		window.location='?page=flight-booking/booking-information';
	}

  	render() {
  		var popUp = "";
  		if (this.state.secondBuy === true) {
  			var popUp = <FlightPopup handleSubmit={this.handleSubmit} handleCancelSecond={this.handleCancelSecond} firstBuyData={this.state.firstBuyData} secondBuyData={this.state.secondBuyData}  />
  		}
	    return (
	      	<div className="resultSection row">
	      		{popUp}
	      		<FlightResultFilter data={this.state.firstBuyData} firstFlight={this.state.firstBuy} handleCancel={this.handleCancel}  />
				<div className="col-sm-9 RSCol2">
					<div className="col-xs-9 RSCol2Tab">
						<div className="RSCol2Tab1">
							<p>Cheapest</p>
							<p className="RSColTabPrice">Rp. 782,000</p>
							<p className="RSColTabHour">2h 18m</p>
						</div>
						<div className="RSCol2Tab2">
							<p>Recommended</p>
							<p className="RSColTabPrice">Rp. 812,000</p>
							<p className="RSColTabHour">1h 52m</p>
						</div>
						<div className="RSCol2Tab3">
							<p>Fast</p>
							<p className="RSColTabPrice">Rp. 925,000</p>
							<p className="RSColTabHour">1h 13m</p>
						</div>
					</div>
					<div className="col-xs-3">
						<div className="RSCol2Caption">
							Choose<br />
							Return Flight
						</div>
					</div>
					{
			            flightList.map(list => {
			                return (
			                    <div className="col-xs-12 RSCol2Result" data-toggle="collapse" data-target={"#"+list.id}>
									<div className="RSCol2Caret">
										<span className="caret"></span>
									</div>
									<div className="RSCol2Flight">
										<img src="assets/images/garuda-icon.png" />
										<div className="RSCol2Departure">
											<p className="RSCol2DepartureStartTime">{moment(list.takeoffTime).format('HH:mm')}</p>
											<p className="RSCol2DepartureStartDate">{moment(list.takeoffTime).format('D MMMM, ddd')}</p>
											<p className="RSCol2DepartureStartPlace">{list.takeoffCity}</p>
										</div>
										<div className="RSCol2Line">
											<span className="glyphicon glyphicon-plane"></span> <hr /> <span className="RSCol2DepartureSpan">o</span>
										</div>
										<div className="RSCol2Arrival">
											<p className="RSCol2DepartureStartTime">{moment(list.landingTime).format('HH:mm')}</p>
											<p className="RSCol2DepartureStartDate">{moment(list.landingTime).format('D MMMM, ddd')}</p>
											<p className="RSCol2DepartureStartPlace">{list.landingCity}</p>
										</div>
										<div id={list.id} className="RSCol2Collapse collapse">
											<div className="RSCol2Collapse2">
												<h5><b>Depart: {moment(list.landingTime).format('ddd, DD MMMM YYYY')}</b></h5>
												<hr />
												<div className="row">
													<div className="col-xs-5">
														<span className="glyphicon glyphicon-briefcase"></span> 20Kg included in the price
													</div>
													<div className="col-xs-7">
														Hand luggage: 1 piece up to 7Kg. per person*
													</div>
												</div>
												<hr />
												<div className="row">
													<div className="col-xs-8">
														<div className="RSCol2CollapseFlightTime">
															<p>{moment(list.takeoffTime).format('HH:mm')}</p>
															<p className="RSCol2CollapseFlightTimeP2">{moment(list.landingTime).format('HH:mm')}</p>
														</div>
														<div className="RSCol2CollapsePoint">
															<p className="RSCol2DepartureSpan">o</p>
															<div className="RSCol2CollapsePointLine"></div>
															<p className="RSCol2DepartureSpan RSCol2DepartureSpanFlightTime">o</p>
														</div>
														<div className="RSCol2CollapseFlightDestination">
															<p>{moment(list.takeoffTime).format('ddd, DD MMM YYYY')}. {list.takeoffProvince}, {list.takeoffAirport}</p>
															<span className="RSCol2CollapseFlightHour"><span className="glyphicon glyphicon-time"></span> {list.flightTime}</span>
															<p className="RSCol2CollapseFlightDestinationDep">{moment(list.landingTime).format('ddd, DD MMM YYYY')}. {list.landingProvince}, {list.landingAirport}</p>
														</div>
													</div>
													<div className="col-xs-4">
														<div className="RSCol2CollapseFlightInformation">
															<p>Flight Information</p>
															<p>Aircraft: <span>Any</span></p>
															<p>Flight: {list.flight}</p>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
									<div className="RSCol2Price">
										<p className="RSCol2PriceText">Rp. {numeral(list.price*list.person).format('0,0')}</p>
										<p className="RSCol2PriceDetail">1 Ticket: Rp. {numeral(list.price).format('0,0')}</p>
										<button onClick={(e) => this.handleBuy(e, list)} className="btn RSCol2PriceButton" type="submit">BUY</button>
									</div>
								</div>
			                )
			            })
			        }
				</div>
			</div>
	    );
  	}
}

class FlightResultFilter extends React.Component{
	render(){
		var firstFlightLayout = '';
		if(this.props.firstFlight === true){
			firstFlightLayout = <DepartureFlight handleCancel={this.props.handleCancel} data={this.props.data} />
		}
		return(

			<div className="col-sm-3">
				{firstFlightLayout}
				<div className="RSCol1">
					<p className="RSCol1Caption">Popular filters</p>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Direct flights only</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Baggage includes only</label>
					</div>
					<hr />
					<p className="RSCol1Caption">Baggage</p>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Baggage includes</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Baggage not includes</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>No Information</label>
					</div>
					<hr />
					<p className="RSCol1Caption">Max connections</p>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Direct flights</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>1 connections</label>
					</div>
					<hr />
					<p className="RSCol1Caption">Departure / Arrival time</p>
					<div className="RSCol1RangeDiv">
						<p className="RSCol1CaptionSub">DEPART</p>
						<div className="checkbox">
							<label>Departure: <input id="departDeparture" type="text" /></label>
						</div>
						<div className="checkbox">
							<label>Arrival: <input id="departArrival" type="text" /></label>
						</div>
					</div>
					<div className="RSCol1RangeDiv">
						<p className="RSCol1CaptionSub">RETURN</p>
						<div className="checkbox">
							<label>Departure: <input id="returnDeparture" type="text" /></label>
						</div>
						<div className="checkbox">
							<label>Arrival: <input id="returnArrival" type="text" /></label>
						</div>
					</div>
					<hr />
					<p className="RSCol1Caption">Airports</p>
					<div className="RSCol1RangeDiv">
						<p className="RSCol1CaptionSub">Jakarta:</p>
						<div className="checkbox">
							<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
						</div>
						<div className="checkbox">
							<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Soekarno-Hatta (CKG)</label>
						</div>
					</div>
					<div className="RSCol1RangeDiv">
						<p className="RSCol1CaptionSub">Denpasar Bali:</p>
						<div className="checkbox">
							<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
						</div>
						<div className="checkbox">
							<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Ngurah Rai (DPS)</label>
						</div>
					</div>
					<hr />
					<p className="RSCol1Caption">Carriers</p>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>All</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Garuda Indonesia</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Indonesia AirAsia</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Malaysia Airlines</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Skystar Airways</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Lion Air</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Citilink</label>
					</div>
					<div className="checkbox">
					  	<label><input type="checkbox" value="" /><span className="cr"><i className="cr-icon glyphicon glyphicon-ok"></i></span>Singapore Airlines</label>
					</div>
				</div>
			</div>
			
		);
	}
}

class DepartureFlight extends React.Component{
	render(){
		return(
			<div className="RSCol1">
				<p className="RSCol2Caption">
					Departure Flight
				</p>
				<img className="RSColImgLeft" src="assets/images/garuda-icon.png" />
				<div className="row">
					<div className="col-xs-6">
						<span className="RSCol2DepartureStartTime">{moment(this.props.data.takeoffTime).format('HH:mm')}</span><br />
						<span className="RSCol2DepartureStartDate">{this.props.data.takeoffProvince}, {this.props.data.takeoffAirport}</span>
					</div>
					<div className="col-xs-6 text-right">
						<span className="RSCol2DepartureStartTime">{moment(this.props.data.landingTime).format('HH:mm')}</span><br />
						<span className="RSCol2DepartureStartDate">{this.props.data.landingProvince}, {this.props.data.landingAirport}</span>
					</div>
				</div>
				<br />
				<div>
					<b>Duration:</b><br />
					{this.props.data.flightTime}, Direct
				</div>
				<br />
				<div>
					<b>Departure Flight Price:</b><br />
					(Inclusive of all taxes and surecharges)
				</div>
				<br />
				<span className="RSColLeftPrice">Rp. {numeral(this.props.data.price*this.props.data.person).format('0,0')}</span>
				<button className="RSCol1ChangeDeparture" onClick={this.props.handleCancel}>
					Change departure flight
				</button>
			</div>
		);
	}
}

class FlightPopup extends React.Component{
	render(){
		return(
			
      		<div className="pop-modal">
			  	<div className="pop-modal-content">
			  		<span className="pop-modal-caption">Round trip flight detail</span><br /><br />
			    	{/*<span className="pop-close" onClick={this.props.handleCancelSecond}>&times;</span>*/}
			    	<div className="row">
			    		<div className="col-xs-3">
			    			<p class="pop-modal-p1">Departure</p>
			    			<p class="pop-modal-p2">{moment(this.props.firstBuyData.takeoffTime).format('ddd, DD MMMM YYYY')}</p>
			    		</div>
			    		<div className="col-xs-3">
			    			<p class="pop-modal-p1">Garuda Indonesia</p>
			    			<p class="pop-modal-p2">Business</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">{moment(this.props.firstBuyData.takeoffTime).format('HH:mm')}</p>
			    			<p class="pop-modal-p2">{this.props.firstBuyData.takeoffProvince} ({this.props.firstBuyData.takeoffAirport})</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">{moment(this.props.firstBuyData.landingTime).format('HH:mm')}</p>
			    			<p class="pop-modal-p2">{this.props.firstBuyData.landingProvince} ({this.props.firstBuyData.landingAirport})</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">Total duration</p>
			    			<p class="pop-modal-p2">{this.props.firstBuyData.flightTime}</p>
			    		</div>
			    	</div>
			    	<hr />
			    	<div className="row">
			    		<div className="col-xs-3">
			    			<p class="pop-modal-p1">Departure</p>
			    			<p class="pop-modal-p2">{moment(this.props.secondBuyData.takeoffTime).format('ddd, DD MMMM YYYY')}</p>
			    		</div>
			    		<div className="col-xs-3">
			    			<p class="pop-modal-p1">Garuda Indonesia</p>
			    			<p class="pop-modal-p2">Business</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">{moment(this.props.secondBuyData.takeoffTime).format('HH:mm')}</p>
			    			<p class="pop-modal-p2">{this.props.secondBuyData.takeoffProvince} ({this.props.secondBuyData.takeoffAirport})</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">{moment(this.props.secondBuyData.landingTime).format('HH:mm')}</p>
			    			<p class="pop-modal-p2">{this.props.secondBuyData.landingProvince} ({this.props.secondBuyData.landingAirport})</p>
			    		</div>
			    		<div className="col-xs-2">
			    			<p class="pop-modal-p1">Total duration</p>
			    			<p class="pop-modal-p2">{this.props.secondBuyData.flightTime}</p>
			    		</div>
			    	</div>
			    	<div className="row">
			    		<div className="col-xs-6">
			    			<p className="pop-modal-p3">Rp. {numeral(this.props.firstBuyData.price+this.props.secondBuyData.price).format('0,0')} / Person</p>
			    		</div>
			    		<div className="col-xs-6 text-right">
			    			<button onClick={this.props.handleSubmit} className="pop-modal-button">Book</button>
			    		</div>
			    	</div>
			  	</div>
			</div>
		);
	}
}

ReactDOM.render(
  <FlightResult />,
  document.getElementById('resultSection')
);